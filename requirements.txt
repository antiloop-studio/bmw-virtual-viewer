bottle==0.12.18
dialogflow==1.0.0
python-dotenv==0.14.0
beautifulsoup4==4.9.1
lxml==4.5.2
boto3==1.15.16
requests-toolbelt==0.9.1
