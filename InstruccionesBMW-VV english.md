# Estructura del proyecto

## Backend

The backend that is in place in teporary. It has to be optimized for production. 

It's developed with bottle (pyton) and is located in the server/ folder:
```
- cors.py           to deactivate CORS
- dialogflow.py     main dialog flow comunication functions
- server.py         server
- views.py          views
[.env]
[credentials/]
```

#### Backend environement and authenitification

On the server,  create a `.env` file with the credentials and server environment variables (the will change depending on the environement):
```
GOOGLE_APPLICATION_CREDENTIALS="server/credentials/BMW-AR-TEST2-4880d5926cc2.json"
SERVER_HOST="localhost"
SERVER_PORT=8000
DIST_FOLDER=public
```

The GOOGLE_APPLICATION_CREDENTIALS variable should point to the to the file with the Google Cloud authentification variable (chosen athentification method). The file will have the follwing structure: 
```
{
  "type": "service_account",
  "project_id": "bmw-ar-dmodjf",
  "private_key_id": "4860c92a983447005deb9ff6ad72d48a9061f9c7",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADA...",
  "client_email": "dialogflow-okudhv@bmw-ar-dmodjf.iam.gserviceaccount.com",
  "client_id": "110340257959660048768",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/..."
}
```

#### API

The request from the front end to the backend have to pass the following parameters:
```
project               el project de dialogflow, por defecto 'bmw-ar-test2-rssdia'
session               la session del usuario, (para identificarlo)
text, file, o event   el texto, audio o evento que hay que triggear
locale                el idioma y localización (por defecto 'en-US')
```

#### Dialogflow audio response

DialogFlow request normaly return an audiofile which will have to be play in the app. 
In order to do this, save the binary code returned by the DialogFlow API to a file, and send the path to the frontend. This is handled by the function `compose_dialogflow_response()` in dialogflow.py. The audio is saved in the folder public/audio/.

To save a bit of resource, if the audio is already saved on the server, it will not be saved again. [It is recommended to delete the existing audio files on the server when changing any dialogflow configuration].

### Frontend

The project is developed with Vue (v2) and the business logic is implemented in Vuex modules and in some components. 

Please note that the initial developer, who placed the foundation of the app wasn't fully utilizing Vue's mechanism and the functional paradigm. So it requires a little shift in the approach to understand the system.
Optimally most of the business logic should be in the Vuex modules, but many information gets to the UI by creating a `getter` for the given feature, like showing an `ExternalLink` (`shouldShowExternalLink`) which computes the props, and then the `MainViewComponent` `watch`es this getter's value and at the end it will result either in an `enableComponent` or `disableComponent` dispatch, which brings us to the next point.

## Displaying and hiding components

Every component is added in the `App` component, either with `v-if` or with `:isActive` prop. They are mostly individual components with no relation to each other. So placing components in relation to another is cumbersome, mostly they are `position: absolute;` and placed to look good.

The way to show a component is to dispatch `enableComponent` action (`ViewsState`) with the argument of the component's name, eg `dispatch('enableComponent', 'ExternalLink')`. Additionally, "props" are handled with an action called `setComponentContent`. Dispatching this (preferably before `enableComponent`) with an object with the format of `{component: string, content: object}` will store the `content` as a state variable in `ViewsState` module. 
This state is then mapped to the component as a `computed` property. 

*Important:* In order for this to be reactive, a state variable needs to be declared by this name for the component. 
```
  this.$store.dispatch('setComponentContent', {
    component: 'ExternalLink',
    content: payload,
  });
  this.$store.dispatch('enableComponent', 'ExternalLink');
```

```
  computed: {
    externalLinkProps() {
      return this.$store.state.ViewsState.ExternalLink; // or just use mapState
    },
  //..
  }
```

Another important thing. Most components use a mixin called `ComponentMixin`. The main reason of this is to enable animations. It decorates the components with an `isActive` data variable, which is initially set to false, but 50ms after mounting, it is being set to `true`. Great majority of animations are done with a conditional class `is-active` which obviously depends on `isActive`. 
This enables the CSS `transition`s to "animate" from initial value to target value. (which is usually from pushed out state (`bottom: -500px`) to a bottom value, with which the component is visible, this emulates a "slide-in" kinda effect)
This way `v-if`s can be used and the animations will work. 
Another responsibility of the `ComponentMixin` is to react to the (templated) `Bus` event of `inactive<ComponetnName>` event. This is used to set the `isActive` to false, so hiding the component. (Keep in mind this won't remove the component from the DOM, also not equal to `disableComponent`)


# BottomMenuComponent

Note for the above point: There is a specific event listener for `inactiveBottomMenuComponent` in `BottomMenuComponent` which also `disableComponent`'s itself after 300ms.

BottomMenuComponent servers as a container for all the components inside `src/components/menu-bottom/content/`, and it doesn't make sense to just show the `BottomMenuComponent`. The way to show a "subcomponent" is following the above mentioned "props" approach.

```
  dispatch('setComponentContent', {
    component: 'BottomMenuComponent', // container
    content: 'BottomMenuCharging', // actual component
  });
  dispatch('enableComponent', 'BottomMenuComponent'); // activate container
```

## User journey / experience

The heart of the app is the `JourneyState`. This module is the "playlist" of the certain interactions prompted to the user as time passes. There is a "timer" which proceeds and triggers the "journey steps". These steps are mainly `launchEvent` calls for dialogflow, and sometimes showing components as well. The journey can be paused as well (see `shouldJourneyBePaused` getter)

Two crucial state variables are `currentAudio` of `PlayerState` module and the `lastIntent` of the `ApiState` module.
These two are used extensively to drive forward the experience. For example, all the `BottomMenuX` components are awaiting "events" like "dialogflow voice line has finished playing" (`currentAudio === undefined`) specified by `lastIntent`. 


## Cookies

There are two `index.html` files. One in `src/` and another in `src/cookie_policy/`. Both include `<script defer src="https://bmw-eprivacy-staging.thisissecure.net/static/bmw-eprivacy-bundle.js"> </script>` script, which only has an effect on the app when it is served under an authorized/whitelisted domain configured by bmw. So when running the app locally it won't load.

### Variables de entorno

Create a new files .env.dev, .end.pro, end.local for each environnement (local, development, production).
The should contain the follwing variables (with different values depending on the file)
```
VUE_APP_EIGHTWALL_KEY=pOx4VGj8ghNXAk3hlk9mKGsav6DxUDP8oE9s12TneKCh8RaAQE0RVrayAbpIsBEwfpuI6L
VUE_APP_API_URL=https://antiloop-test-api.dataira.com
VUE_APP_DIALOGFLOW_PROJECT=bmw-ar-dmodjf
VUE_APP_DIALOGFLOW_LOCALE=en-GB
```

#### Status managment Gestión de los estados

The functions to manage de state and other asociated services are in the store folder.
```
api/            backend comunication 
bus/            genereal bus to trigger certain events
camera/         device camera access
microphone/     microphone access and audio recording
player/         play audio and control volume (mute on/off)
journey/        the user journey sequencer
views/          handling views and components
webgl/          communication with webGL
```

#### Recording audio

Each device (android/iphone) records audio its own way. The recording solution is taken from this repository: https://github.com/kaliatech/web-audio-recording-tests-simpler
Previously the [WebAudioRecorder.js](https://github.com/higuma/web-audio-recorder-js) library was used, but it had issues with lower-end iPhones when the WebGL module was also loaded (so the app was using the phone's resources exhaustively). Be cautious with modifying this prat of the app.
If there is an issue, maybe experiment with changing the buffer size. 


#### Integration with the WebGL repository (bmw)

For the bmw repository (webGL) intergation to work properly, it is necessary to modify the route in the assetsLoaders.js file:

These lines:
```
this.setupLoader(this.gltfLoader, './models/', '');
this.setupLoader(this.textureLoader, './images/textures/', '');
this.setupLoader(this.rgbeLoader, './images/textures/', '');
```

Have to be changed with these: 
```
this.setupLoader(this.gltfLoader, './armanager/models/', '');
this.setupLoader(this.textureLoader, './armanager/images/textures/', '');
this.setupLoader(this.rgbeLoader, './armanager/images/textures/', '');
```

Once the update in completed and the build is done, copy the files from the folder [bmw repo]/AppV7/dist/ to [
BMW Virtual Viewer repo]/public/armanager/ (the file [bmw repo] /build.sh is used for this)

Keep in mind that the device's cache has to be erased before loading the web page, in order to properly load the latest webGL files.

#### Other notes

- Instructions to build the development environment and deploy are in the README.md. They are slightly out of date (not much)

- The DevMenuComponent.vue component is a general state console, that displays the gerenal state of the app. From the console, you can play the last audio recorded by the user. This helps in checking the quality of the audio sent to dialogflow. 

- The 8thwall files must be inserted into the html once the camera, microphone and accelerometer permissions have been granted, as these files will request them automatically.

- The files deploy.sh had been created to simplify the deployment. It runs on the server and downloads the latest repo changes, deletes the audios and restarts the processes (I use uwsgi y nginx)

- Main styles are in the folder styles/. Component-specific styles are in each componente file.

- Current front end dependencies (a few can be removed) :
```
"axios": "^0.19.2",
"babylonjs": "4.1.0",                 // used for webgl
"babylonjs-loaders": "4.1.0",         // used for webgl
"core-js": "3.6.5",
"dialogflow": "^1.2.0",               // can be removed
"flickity": "^2.2.1",                 // used for the slider
"howler": "^2.2.0",                   // used to play audio
"three": "^0.118.3",                  // can be removed (I think)
"uuid": "^8.1.0",                     // used for the user session
"vue": "2.6.11",
"vue-axios": "^2.1.5",
"vue-directive-long-press": "^1.1.0", // used to manage press and hold of chatbot button
"vue-router": "3.2.0",
"vue-slick-carousel": "^1.0.6",       // can be removed
"vue-slider-component": "^3.2.3",     // can be removed
"vuex": "3.4.0",
"web-audio-recorder-js": "0.0.2"      // to play the audio
```
