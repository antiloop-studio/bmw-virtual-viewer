# BMW Virtual Viewer

On-working project

Dependencies:
- Frontend: 8thwall, Threejs, Vue, Node
- Backend: Python, Bottle

## Project structure
```
- dist/                         -> Build files ready to deploy. It's emptied on every build
- public/                       -> Public assets to use in the project. Place your static files here
  |- ...
  |- index.html                 -> Main html file template to inject dependencies
- server/                       -> Development server files
- src/                          -> Frontend development folder
  |- assets/                    -> Assets folder
     |- armanager/              -> AR Manager dist folder
  |- components/                -> Components folder
     |- ...
     |- views/                  -> Views folder
  |- store/                     -> State folder
  |- styles/                    -> Styles (CSS) folder
  |- App.vue                    -> App main component
  |- main.js                    -> App entry file
```

## Project setup

#### Frontend

0. Clone the repository and go to the project's folder

1. Install frontend dependencies:
```
npm install
```

2. Create a file `.env.local` on the project's root and set the [app key from 8thwall](https://www.8thwall.com/antiloop/bmw-test/settings).
```
VUE_APP_EIGHTWALL_KEY=pOx4VGj8ghNXAk3hlk9mKGsav6DxUDP8...
```

##### AR Manager Integration

Since ARManager isn't a shared repository, in order to update it you must copy all files from [bmw repo](https://bitbucket.org/antiloop-studio/bmw/src/master/) `/AppV5/dist` folder and replace the files in (this repository) `src/assets/armanager` folder.

#### Backend

1. Create a virtualenv and activate it
```
virtualenv -p `which python3` env
. env/bin/activate
```

2. Install dependencies
```
pip install bottle dialogflow python-dotenv
```

3. Get your Google application credentials file [here](https://console.cloud.google.com/iam-admin/serviceaccounts?project=bmw-ar-test2-rssdia) (Project name: Dialogflow IntegrationsHG) and place it file under `server/credentials/`. It will be something like:
```
{
  "type": "service_account",
  "project_id": "bmw-ar-test2-rssdia",
  "private_key_id": "...",
  "private_key": "...",
  "client_email": "...",
  "client_id": "...",
  "auth_uri": "...",
  "token_uri": "...",
  "auth_provider_x509_cert_url": "...",
  "client_x509_cert_url": "..."
}

```

4. Create a file `server/.env` with the folowing variables:
```
GOOGLE_APPLICATION_CREDENTIALS="server/credentials/YOUR_CREDENTIALS_FILE.json"
SERVER_HOST="localhost"
SERVER_PORT=8000
DIST_FOLDER=public
```
> SERVER_HOST and SERVER_PORT are the host and port to bind the server

#### Empty permissions

This application stores multiple permissions, such as cookies, microphone, etc. To fully test the app's behaviur, you may need to empty allowed permissions:

- Unset cookies permisssions (stored on localStorage): Open you browser's console (F12) and execute:
```
localStorage.removeItem('cookies-allowed');
```

- Unset microphone access:
```
Chrome: go to chrome://settings/content/microphone and unset the url (local or production)
```

- Unset camera access:
```
Chrome: go to chrome://settings/content/camera and unset the url (local or production)
```

- Review all app permissions:
```
Chrome: go to chrome://settings/content/siteDetails?site=http%3A%2F%2Flocalhost%3A8080
```

## Run project on development mode

1. Start the backend:
```
python server/server.py
```

2. Start the frontend:
```
npm run serve
```
> To view the project on your mobile, you must be connected to the same network (wifi). Once done, open the Vue external url. It will be something like https://192.168.X.X:8080.

## Deploy on production server

To build the files on dev mode (https://antiloop-test.dataira.com), run:
```
npm run build-dev
```

To build the files on prod mode (https://antiloop.dataira.com), run:
```
npm run build-pro
```

On the server, you need to pull the master branch, clear the audio files and restore the daemos. To do so, run:
```
bash deploy.sh
```

## Troubleshooting

- If the terminal shows some errors, try running the linter fix:
```
npm run lint --fix
```

- If the console shows some invalid google cloud permissions, try to place your absolute path to your google credentials on `server/.env`:
```
GOOGLE_APPLICATION_CREDENTIALS="/your/path/to/YOUR_CREDENTIALS_FILE.json"
```

- 8thWall requires https to load. You can serve Vue files on localhost and https uncommenting `vue.config.js` https option. Note that in your browser you have to write https://localhost:8080 (with http**s**) 

<hr />


## Application Architecture

<div align="center">
  <img alt="Architecture" src="/architecture.jpg" />
</div>
