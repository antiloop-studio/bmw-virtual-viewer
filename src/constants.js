const X1ConfiguratorExternalLink = {
  title: 'BMW configurator',
  url: 'https://configure.bmw.co.uk/en_GB/configid/p5u6c7h7?tl=grp-vivi-vivi-pro-mn-p020-.-x1person-.-.',
};
const X3ConfiguratorExternalLink = {
  title: 'BMW configurator',
  url: 'https://configure.bmw.co.uk/en_GB/configid/x1h2h3a0?tl=grp-vivi-vivi-pro-mn-p052-.-3tourper-.-.',
};
const X5ConfiguratorExternalLink = {
  title: 'BMW configurator',
  url: 'https://configure.bmw.co.uk/en_GB/configid/u8o8s2m5?tl=grp-vivi-vivi-pro-mn-p024-.-x5person-.-.',
};

const powerModesExternalLink = {
  title: 'Explore Power Modes',
  url: 'https://www.bmw.co.uk/en/topics/discover/electric-and-phev/plug-in-hybrids.html?tl=grp-vivi-vivi-pro-mn-.-.-moreabpw-.-.',
  duration: 7000,
};
const chargingExternalLink = {
  title: 'Explore charging',
  url: 'https://discover.bmw.co.uk/amp/plug-in-hybrids-charging?tl=grp-vivi-vivi-pro-mn-.-.-moreabch-.-.',
  duration: 7000,
};
const rangeExternalLink = {
  title: 'Explore range',
  url: 'https://www.bmw.co.uk/en/topics/discover/electric-and-phev/plug-in-hybrids.html?tl=grp-vivi-vivi-pro-mn-.-.-morerang-.-.&tl=sea-gl-%7BCampaignName%7D-mix-miy-.-sech-%7BAdGroupName%7D-.e-bmw%20phev-.-&gclid=Cj0KCQjw0Mb3BRCaARIsAPSNGpVQdhgFIu9j5SMavkYNFtv8mezqC0Gbf_FSXxKy0HGyZsUezWV8io0aApeBEALw_wcB&gclsrc=aw.ds',
  duration: 7000,
};
const registerInterestExternalLink = {
  title: 'Register your interest',
  url: 'https://www.bmw.co.uk/en/topics/discover/forms/pdi_bmw_i2767_dc.html?tl=grp-vivi-vivi-pro-mn-.-.-uptodate-.-.',
};
const bmwIndividualExternalLink = {
  title: 'BMW Individual',
  url: 'https://discover.bmw.co.uk/iframes/individual-visualiser?tl=grp-vivi-vivi-pro-mn-p024-.-discbmwi-.-.',
};
const discoverThePhevRangeExternalLink = {
  title: 'Discover the PHEV range',
  url: 'https://www.bmw.co.uk/en/topics/discover/electric-and-phev/plug-in-hybrids.html?tl=grp-vivi-vivi-pro-mn-.-.-discphev-.-.',
};
const moreAboutX1PricesExternalLink = {
  title: 'More about prices',
  url: 'https://configure.bmw.co.uk/en_GB/configid/f7c7u7u0?tl=grp-vivi-vivi-pro-mn-p020-.-x1person-.-.',
};
const moreAbout330EPricesExternalLink = {
  title: 'More about prices',
  url: 'https://configure.bmw.co.uk/en_GB/configid/s5a7s4a9?tl=grp-vivi-vivi-pro-mn-p052-.-3tourper-.-.',
};
const moreAboutX5PricesExternalLink = {
  title: 'More about prices',
  url: 'https://configure.bmw.co.uk/en_GB/configid/x6o4g5n6?tl=grp-vivi-vivi-pro-mn-p024-.-x5person-.-.',
};
const moreAboutBmwPhevExternalLink = {
  title: 'More about BMW PHEV',
  url: 'https://www.bmw.co.uk/en/topics/owners/service-workshop/warranties.html?tl=grp-vivi-vivi-pro-mn-.-.-batteryl-.-.',
};

module.exports = Object.freeze({
  /* Views */
  views: [{
    name: 'welcome',
    view: 'HomeComponent',
    times: {
      nextViewSleep: 1000,
    },
  }, {
    name: 'permissions-camera',
    view: 'PermissionsCameraComponent',
  }, {
    name: 'permissions-motion',
    view: 'PermissionsMotionComponent',
  }, {
    name: 'permissions-micro',
    view: 'PermissionsMicroComponent',
  }, {
    name: 'permissions-disabled',
    view: 'PermissionsDisabledComponent',
  }, {
    name: 'permissions-desktop',
    view: 'PermissionsDesktopComponent',
  }, {
    name: 'car-selection',
    view: 'CarSelectionComponent',
    times: {
      selectCarSleep: 100,
      selectedCarSleep: 250,
      destroySleep: 250,
    },
  }, {
    name: 'main-view',
    view: 'MainViewComponent',
    times: {},
  }],

  animationIds: {
    xray: 0,
    charge: 1,
    range: 2,
  },

  animations: [
    {
      triggeredByIntent: ['7.2.1', '7.2.2', '7.2.3'],
      animationId: 0,
    },
    {
      triggeredByIntent: ['8.5.1', '8.5.2', '8.5.3'],
      animationId: 2,
    },
    {
      triggeredByIntent: ['10.5.1', '10.5.2', '10.5.3'],
      animationId: 1,
    },
  ],

  registerInterestExternalLink,

  externalLinkDuringIntent: {
    '0.13': registerInterestExternalLink,
    '16.1': bmwIndividualExternalLink,
    '16.4': discoverThePhevRangeExternalLink,

    '5.3.1': X1ConfiguratorExternalLink,
    '6.3.1': X1ConfiguratorExternalLink,

    '5.3.2': X3ConfiguratorExternalLink,
    '6.3.2': X3ConfiguratorExternalLink,

    '5.3.3': X5ConfiguratorExternalLink,
    '6.3.3': X5ConfiguratorExternalLink,

    '16.7.1': moreAboutX1PricesExternalLink,
    '16.7.2': moreAbout330EPricesExternalLink,
    '16.7.3': moreAboutX5PricesExternalLink,
    '16.8': moreAboutBmwPhevExternalLink,

  },

  externalLinkAfterIntent: {
    '7.2.1': powerModesExternalLink,
    '7.2.2': powerModesExternalLink,
    '7.2.3': powerModesExternalLink,

    '8.5.1': rangeExternalLink,
    '8.5.2': rangeExternalLink,
    '8.5.3': rangeExternalLink,

    '10.5.1': chargingExternalLink,
    '10.5.2': chargingExternalLink,
    '10.5.3': chargingExternalLink,

    '0.13': { ...registerInterestExternalLink, duration: 3000 },
    '16.1': { ...bmwIndividualExternalLink, duration: 3000 },
    '16.4': { ...discoverThePhevRangeExternalLink, duration: 3000 },

    '5.3.1': { ...X1ConfiguratorExternalLink, duration: 3000 },
    '6.3.1': { ...X1ConfiguratorExternalLink, duration: 3000 },

    '5.3.2': { ...X3ConfiguratorExternalLink, duration: 3000 },
    '6.3.2': { ...X3ConfiguratorExternalLink, duration: 3000 },

    '5.3.3': { ...X5ConfiguratorExternalLink, duration: 3000 },
    '6.3.3': { ...X5ConfiguratorExternalLink, duration: 3000 },

    '16.7.1': { ...moreAboutX1PricesExternalLink, duration: 3000 },
    '16.7.2': { ...moreAbout330EPricesExternalLink, duration: 3000 },
    '16.7.3': { ...moreAboutX5PricesExternalLink, duration: 3000 },
    '16.8': { ...moreAboutBmwPhevExternalLink, duration: 3000 },
  },

  relatedIntents: [
    [
      '0.0.0',
      '7.1',
      '7.1.3',
      '7.2.1', '7.2.2', '7.2.3',
      '7.1.1', '7.1.1.1', '7.1.2.1', '7.1.3.1',
      '7.1.2', '7.1.1.2', '7.1.2.2', '7.1.3.2',
    ],
    [
      '0.0.0',
      '8.0',
      '8.0.0',
      '8.1',
      '8.2',
      '8.3',
      '8.1.1', '8.1.2', '8.1.3',
      '8.4.1', '8.4.2', '8.4.3', '8.4.4',
      '8.5',
      '8.5.1', '8.5.2', '8.5.3',
      '8.9',
    ],
    [
      '0.0.0',
      '10.1',
      '10.1.0',
      '10.2',
      '10.3',
      '10.4.1',
      '10.4.2',
      '10.4.3',
      '10.5',
      '10.5.1', '10.5.2', '10.5.3',
      '10.9',
    ],
    [
      '0.0.0',
      '11.1.3',
      '11.4',
      '11.1.1', '11.1.1.1', '11.1.2.1', '11.1.3.1',
      '11.1.2', '11.1.1.2', '11.1.2.2', '11.1.3.2',
    ],
  ],

  legalTerms: [
    {
      triggeringIntents: ['10.5.1', '10.5.2', '10.5.3'],
      text: '*As of 11 November 2020, there are 12,694 charging locations in the UK according to https://www.zap-map.com/statistics/',
      duration: 23000,
    },
    {
      triggeringIntents: ['4.6.2', '4.6.3', '8.5.1', '8.5.2', '8.5.3'],
      text: '*Range figures are provided for comparability purposes. They may not reflect real life driving results, which will depend upon a number of factors including accessories fitted (post-registration), variations in weather, driving styles and vehicle load. The vehicle needs to be fully recharged.',
      duration: 23000,
    },
    {
      triggeringIntents: ['14.3.1.11', '14.3.2.11', '14.3.3.11'],
      text: '*Fuel consumption figures are provided for comparability purposes. They may not reflect real life driving results, which will depend upon a number of factors including accessories fitted (post-registration), variations in weather, driving styles and vehicle load. The vehicle needs to be fully recharged.',
      duration: 23000,
    },
    {
      triggeringIntents: ['13.4'],
      text: '*Car shown may not be to scale.',
      duration: 7000,
    },
  ],

  bmwTechnologyIntroIntents: ['18.1.1', '18.1.2', '18.1.3', '18.1.4'],

  bmwTechnologyIntents: {
    'X1': ['14.3.1.6', '14.3.1.12'],
    '330E': ['14.3.2.5', '14.3.2.6', '14.3.2.7', '14.3.2.10'],
    'X5': ['14.3.3.5', '14.3.3.6', '14.3.3.7', '14.3.3.10'],
  },

  bmwTechnologies: [
    {
      intent: '14_3_1_6',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/X1/14.3.1.6.jpg',
          text: 'Head-Up Display.',
        }],
      },
    },
    {
      intent: '14_3_1_12',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/X1/14.3.1.12.jpg',
          text: 'Traffic Updates.',
        }],
      },
    },

    {
      intent: '14_3_2_5',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/330E/14.3.2.5.jpg',
          text: 'Intelligent Personal Assistant.',
        }],
      },
    },
    {
      intent: '14_3_2_6',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/330E/14.3.2.6.jpg',
          text: 'Head-Up Display.',
        }],
      },
    },
    {
      intent: '14_3_2_7',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/330E/14.3.2.7.jpg',
          text: 'Gesture Control.',
        }],
      },
    },
    {
      intent: '14_3_2_10',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/330E/14.3.2.10.jpg',
          text: 'Connected Music.',
        }],
      },
    },

    {
      intent: '14_3_3_5',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/X5/14.3.3.5.jpg',
          text: 'Intelligent Personal Assistant.',
        }],
      },
    },
    {
      intent: '14_3_3_6',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/X5/14.3.3.6.jpg',
          text: 'Head-Up Display.',
        }],
      },
    },
    {
      intent: '14_3_3_7',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/X5/14.3.3.7.jpg',
          text: 'Gesture Control.',
        }],
      },
    },
    {
      intent: '14_3_3_10',
      infoPanelProps: {
        pages: [{
          image: 'img/2dcontent/X5/14.3.3.10.jpg',
          text: 'Connected Music.',
        }],
      },
    },
  ],

  /* Models */
  models: [{
    id: 1,
    code: 'X1',
    name: 'The X1',
    model: 'BMW X1',
    analyticsName: 'X1',
    event: '3_1_1',
    intent: '3.1.1',
    image: 'img/models/x1/x1.png',
    colours: [
      { image: 'img/models/x1/colours/Alpine_White.jpg', name: 'Alpine White' },
      { image: 'img/models/x1/colours/Black_Sapphire_Metallic.jpg', name: 'Black Sapphire Metallic' },
      { image: 'img/models/x1/colours/Mineral_Grey_Metallic.jpg', name: 'Mineral Grey Metallic' },
      { image: 'img/models/x1/colours/Mineral_White_Metallic.jpg', name: 'Mineral White Metallic' },
      { image: 'img/models/x1/colours/Misano_Blue_Metallic.jpg', name: 'Misano Blue Metallic' },
      { image: 'img/models/x1/colours/Storm_Bay_Metallic.jpg', name: 'Storm Bay Metallic' },
    ],
    alloys: [
      { image: 'img/models/x1/alloys/18.png', name: '19” Double Spoke Style 816 M' },
      { image: 'img/models/x1/alloys/19.png', name: '18” Double Spoke Style 570 M' },
    ],
    upholstery: [
      { image: 'img/models/x1/upholstery/Mocha_Dakota_Leather.jpg', type: 'white', name: 'Mocha Dakota Leather' },
      { image: 'img/models/x1/upholstery/Black_with_Blue_Stitching_Dakota_Leather.jpg', type: 'black', name: 'Black with Blue Stitching Dakota Leather' },
      { image: 'img/models/x1/upholstery/Oyster_with_Grey_Stitching_Dakota_Leather.jpg', type: 'black', name: 'Oyster with Grey Stitching Dakota Leather' },
      { image: 'img/models/x1/upholstery/Black_Dakota_Leather.jpg', type: 'black', name: 'Black Dakota Leather' },
    ],
    trims: [
      { image: 'img/models/x1/trims/Aluminium_Hexagon_with_Estoril_Blue.jpg', name: 'Aluminium Hexagon with Estoril Blue' },
      { image: 'img/models/x1/trims/High-gloss_black_with_Pearl_chrome.jpg', name: 'High-Gloss Black with Pearl Chrome' },
      { image: 'img/models/x1/trims/Brushed_aluminium_with_Pearl_chrome.jpg', name: 'Brushed Aluminium with Pearl Chrome' },
      { image: 'img/models/x1/trims/Fineline_Stream_wood_with_Pearl_chrome.jpg', name: 'Fineline Stream Wood with Pearl Chrome' },
    ],
  }, {
    id: 2,
    code: '330E',
    name: 'The 3',
    model: 'The 3',
    analyticsName: '3 Series',
    event: '3_1_2',
    intent: '3.1.2',
    image: 'img/models/330E/330E.png',
    colours: [
      { image: 'img/models/330E/colours/Black_Sapphire_Metallic.jpg', name: 'Black Sapphire Metallic' },
      { image: 'img/models/330E/colours/Alpine_White.jpg', name: 'Alpine White' },
      { image: 'img/models/330E/colours/Mineral_Grey_Metallic.jpg', name: 'Mineral Grey Metallic' },
      { image: 'img/models/330E/colours/Mineral_White_Metallic.jpg', name: 'Mineral White Metallic' },
      { image: 'img/models/330E/colours/Portimao_Blue_Metallic.jpg', name: 'Portimao Blue Metallic' },
    ],
    alloys: [
      { image: 'img/models/330E/alloys/18_Double_Spoke_Style_790_M.png', name: '18” Double Spoke Style 790 M' },
      { image: 'img/models/330E/alloys/19_Double_Spoke_Individual_Style_793.png', name: '19” Double Spoke Individual Style 793' },
      { image: 'img/models/330E/alloys/19_Double_Spoke_Style_791_M.png', name: '19” Double Spoke Style 791 M' },
    ],
    upholstery: [
      { image: 'img/models/330E/upholstery/Oyster_with_Grey_Stitching_Vernasca_Leather.jpg', type: 'white', name: 'Oyster with Grey Stitching Vernasca Leather' },
      { image: 'img/models/330E/upholstery/Mocha_with_Blue_Stitching_Vernasca_Leather.jpg', type: 'black', name: 'Mocha with Blue Stitching Vernasca Leather' },
      { image: 'img/models/330E/upholstery/Cognac_with_Mocha_Stitching_Vernasca_Leather.jpg', type: 'black', name: 'Cognac with Mocha Stitching Vernasca Leather' },
      { image: 'img/models/330E/upholstery/Black_with_Grey_Stitching_Vernasca_Leather.jpg', type: 'black', name: 'Black with Grey Stitching Vernasca Leather' },
      { image: 'img/models/330E/upholstery/Black_with_Blue_Stitching_Vernasca_Leather.jpg', type: 'black', name: 'Black with Blue Stitching Vernasca Leather' },
    ],
    trims: [
      { image: 'img/models/330E/trims/Aluminium_Tetragon.jpg', name: 'Aluminium Tetragon' },
      { image: 'img/models/330E/trims/Aluminium_Mesheffect.jpg', name: 'Aluminium Mesheffect' },
      { image: 'img/models/330E/trims/Fine-wood_ash_grey-brown_high-gloss.jpg', name: 'Fine-Wood Ash Grey-Brown High-Gloss' },
      { image: 'img/models/330E/trims/Piano_Black.jpg', name: 'Piano Black' },
    ],
  }, {
    id: 3,
    code: 'X5',
    name: 'The X5',
    model: 'BMW X5',
    analyticsName: 'X5',
    event: '3_1_3',
    intent: '3.1.3',
    image: 'img/models/x5/x5.png',
    colours: [
      { image: 'img/models/x5/colours/4.jpeg', name: 'Arctic Grey Brilliant Effect' },
      { image: 'img/models/x5/colours/1.jpeg', name: 'Alpine White' },
      { image: 'img/models/x5/colours/3.jpeg', name: 'Black Sapphire Metallic' },
      { image: 'img/models/x5/colours/6.jpeg', name: 'Mineral White Metallic' },
      { image: 'img/models/x5/colours/7.jpeg', name: 'Phytonic Blue Metallic' },
      { image: 'img/models/x5/colours/5.jpeg', name: 'Carbon Black Metallic' },
      { image: 'img/models/x5/colours/8.jpeg', name: 'Tazanite Blue Metallic' },
      { image: 'img/models/x5/colours/2.jpeg', name: 'Ametrine Metallic' },
    ],
    alloys: [
      { image: 'img/models/x5/alloys/1.jpeg', name: '22” Double-spoke style 742 M' },
      { image: 'img/models/x5/alloys/2.jpeg', name: '22” V-spoke 746 BMW Individual' },
      { image: 'img/models/x5/alloys/3.jpeg', name: '20” Star-spoke style 740 M' },
      { image: 'img/models/x5/alloys/4.jpeg', name: '21” Y-spoke 741 M' },
    ],
    upholstery: [
      { image: 'img/models/x5/upholstery/01_black_vernasca.jpeg', type: 'black', name: 'Black Vernasca Leather' },
      { image: 'img/models/x5/upholstery/02_black_brown_vernasca.jpeg', type: 'black', name: 'Black Vernasca Leather with Contrast Stitching | Brown/Black' },
      { image: 'img/models/x5/upholstery/03_ivory_white_vernasca.jpeg', type: 'white', name: 'Ivory White Vernasca Leather' },
      { image: 'img/models/x5/upholstery/04_coffee_vernasca.jpeg', type: 'dark', name: 'Coffee Full Merino Leather' },
      { image: 'img/models/x5/upholstery/05_white_extended_merino.jpeg', type: 'white', name: 'Ivory White Extended Merino Leather' },
      { image: 'img/models/x5/upholstery/06_coffe_black.jpeg', type: 'dark', name: 'Coffee Extended Merino Leather' },
      { image: 'img/models/x5/upholstery/07_black_individual_merino.jpeg', type: 'black', name: 'Black Full Merino Leather' },
      { image: 'img/models/x5/upholstery/08_ivory_white_night_blue.jpeg', type: 'white', name: 'Ivory White and Night Blue Extended Merino Leather' },
      { image: 'img/models/x5/upholstery/09_merino_ivory_white.jpeg', type: 'white', name: 'Ivory White Full Merino Leather' },
      { image: 'img/models/x5/upholstery/10_merino_coffee.jpeg', type: 'dark', name: 'Coffee Vernasca Leather' },
      { image: 'img/models/x5/upholstery/11_black_full_leather_trim_merino.jpeg', type: 'black', name: 'Black Individual Extended Merino Leather' },
      { image: 'img/models/x5/upholstery/12_trim_ivory_white_blue.jpeg', type: 'white', name: 'Ivory White and Night Blue Full Merino Leather' },
    ],
    trims: [
      { image: 'img/models/x5/trims/01_aluminium_tetragon.jpeg', name: 'Aluminum Tetragon' },
      { image: 'img/models/x5/trims/02_fine-wood_trim.jpeg', name: 'Fineline Stripe Brown High-Gloss' },
      { image: 'img/models/x5/trims/03_piano_black.jpeg', name: 'Individual Piano Black' },
      { image: 'img/models/x5/trims/04_ash_grain_silver_grey.jpeg', name: 'Ash Grain Silver Grey High-Gloss' },
      { image: 'img/models/x5/trims/05_carbon_fibre.jpeg', name: 'Carbon Fibre' },
    ],
  }],

  intents: {
    powerModes: {
      trigger: '7.1',
      starters: ['7.1.1', '7.1.2', '7.1.3'],
      explanations: ['7.2.1', '7.2.2', '7.2.3'],
      explanationIntentEvents: ['7_2_1', '7_2_2', '7_2_3'],
      yes: ['7.1.1.1', '7.1.2.1', '7.1.3.1'],
      no: ['7.1.1.2', '7.1.2.2', '7.1.3.2'],
    },
    range: {
      trigger: '8.1',
      starters: ['8.1.1', '8.1.2', '8.1.3'],
      answerReplies: ['8.0', '8.4.1', '8.4.2', '8.4.3', '8.4.4'],
      explanations: ['8.5.1', '8.5.2', '8.5.3'],
    },
    charging: {
      starters: ['10.1'],
      answerReplies: ['10.4.1', '10.4.2', '10.4.3'],
      explanations: ['10.5.1', '10.5.2', '10.5.3'],
    },
    savings: {
      trigger: '11.1',
      starters: ['11.1.1', '11.1.2', '11.1.3'],
      explanations: ['11.4'],
      yes: ['11.1.1.1', '11.1.2.1', '11.1.3.1'],
      no: ['11.1.1.2', '11.1.2.2', '11.1.3.2'],
    },

    colorComment: '6.1',
    upholsteryComments: ['5.1.1', '5.1.2', '5.1.3'],
    personalizationConclusions: ['5.3.1', '5.3.2', '5.3.3', '6.3.1', '6.3.2', '6.3.3'],
  },

  intentEventsGroupedByModelCode: {
    'X1': {
      midExperienceCarChangeWelcome: '3_3_1',
      welcome: '4_6_1',
      powerModesStarter: '7_1_1',
      powerModesExplanation: '7_2_1',
      rangeStarter: '8_1_1',
      rangeExplanation: '8_5_1',
      chargingStarter: '10_1',
      chargingExplanation: '10_5_1',
      savingsStarter: '11_1_1',
      savingsExplanation: '11_4',

      chooseColorIntro: '6_1_1',
      chooseAlloysPrompt: '6_2_1',
      exteriorPersonalizationConclusion: '6_3_1',
      interiorPersonalizationConclusion: '5_3_1',

      technologyStarter: '18_1_1',

      priceExplanation: '16_7_1',

      lighting: '14_3_1_2',
      parkingAssistant: '14_3_1_3',
      chargingFlap: '14_3_1_4',
      display: '14_3_1_6',
      soundSystem: '14_3_1_8',
      navigation: '14_3_1_9',
      emissions: '14_3_1_11',
      trafficUpdates: '14_3_1_12',
    },
    '330E': {
      midExperienceCarChangeWelcome: '3_3_2',
      welcome: '4_6_2',
      powerModesStarter: '7_1_2',
      powerModesExplanation: '7_2_2',
      rangeStarter: '8_1_2',
      rangeExplanation: '8_5_2',
      chargingStarter: '10_1',
      chargingExplanation: '10_5_2',
      savingsStarter: '11_1_2',
      savingsExplanation: '11_4',

      chooseColorIntro: '6_1_2',
      chooseAlloysPrompt: '6_2_2',
      exteriorPersonalizationConclusion: '6_3_2',
      interiorPersonalizationConclusion: '5_3_2',

      technologyStarter: '18_1_2',

      priceExplanation: '16_7_2',

      lighting: '14_3_2_2',
      parkingAssistant: '14_3_2_3',
      chargingFlap: '14_3_2_4',
      intelligentAssistant: '14_3_2_5',
      display: '14_3_2_6',
      gestureNavigation: '14_3_2_7',
      soundSystem: '14_3_2_8',
      navigation: '14_3_2_9',
      connectedMusic: '14_3_2_10',
      emissions: '14_3_2_11',
    },
    'X5': {
      midExperienceCarChangeWelcome: '3_3_3',
      welcome: '4_6_3',
      powerModesStarter: '7_1_3',
      powerModesExplanation: '7_2_3',
      rangeStarter: '8_1_3',
      rangeExplanation: '8_5_3',
      chargingStarter: '10_1',
      chargingExplanation: '10_5_3',
      savingsStarter: '11_1_3',
      savingsExplanation: '11_4',

      chooseColorIntro: '6_1_3',
      chooseAlloysPrompt: '6_2_3',
      exteriorPersonalizationConclusion: '6_3_3',
      interiorPersonalizationConclusion: '5_3_3',

      priceExplanation: '16_7_3',

      technologyStarter: '18_1_3',

      lighting: '14_3_3_2',
      parkingAssistant: '14_3_3_3',
      chargingFlap: '14_3_3_4',
      intelligentAssistant: '14_3_3_5',
      display: '14_3_3_6',
      gestureNavigation: '14_3_3_7',
      soundSystem: '14_3_3_8',
      navigation: '14_3_3_9',
      connectedMusic: '14_3_3_10',
      emissions: '14_3_3_11',
    },
  },

  generalHotspotIntentToKeywordMap: {
    '14.3.2': 'lighting',
    '14.3.3': 'parkingAssistant',
    '14.3.4': 'chargingFlap',
    '14.3.5': 'intelligentAssistant',
    '14.3.6': 'display',
    '14.3.7': 'gestureNavigation',
    '14.3.8': 'soundSystem',
    '14.3.9': 'navigation',
    '14.3.10': 'connectedMusic',
    '14.3.11': 'emissions',
    '14.3.12': 'trafficUpdates',
  },

  hotspots: {
    // X1
    '14_3_1_2': {
      text: 'Lighting.',
      image: 'img/2dcontent/X1/14.3.1.2.jpg',
    },
    '14_3_1_3': {
      text: 'Parking assistant.',
      image: 'img/2dcontent/X1/14.3.1.3.jpg',
    },
    '14_3_1_4': {
      text: 'Charging Point.',
      image: 'img/2dcontent/X1/14.3.1.4.jpg',
    },
    '14_3_1_6': {
      text: 'Head-Up Display.',
      image: 'img/2dcontent/X1/14.3.1.6.jpg',
    },
    '14_3_1_8': {
      text: 'Sound System.',
      image: 'img/2dcontent/X1/14.3.1.8.jpg',
    },
    '14_3_1_9': {
      text: 'Navigation.',
      image: 'img/2dcontent/X1/14.3.1.9.jpg',
    },
    '14_3_1_11': {
      text: 'Efficiency and Emissions.',
      image: 'img/2dcontent/X1/14.3.1.11.jpg',
    },
    '14_3_1_12': {
      text: 'Traffic Updates.',
      image: 'img/2dcontent/X1/14.3.1.12.jpg',
    },

    // 330E
    '14_3_2_2': {
      text: 'Lighting.',
      image: 'img/2dcontent/330E/14.3.2.2.jpg',
    },
    '14_3_2_3': {
      text: 'Parking assistant.',
      image: 'img/2dcontent/330E/14.3.2.3.jpg',
    },
    '14_3_2_4': {
      text: 'Charging Point.',
      image: 'img/2dcontent/330E/14.3.2.4.jpg',
    },
    '14_3_2_5': {
      text: 'Intelligent Personal Assistant.',
      image: 'img/2dcontent/330E/14.3.2.5.jpg',
    },
    '14_3_2_6': {
      text: 'Head-Up Display.',
      image: 'img/2dcontent/330E/14.3.2.6.jpg',
    },
    '14_3_2_7': {
      text: 'Gesture Control.',
      image: 'img/2dcontent/330E/14.3.2.7.jpg',
    },
    '14_3_2_8': {
      text: 'Sound System.',
      image: 'img/2dcontent/330E/14.3.2.8.jpg',
    },
    '14_3_2_9': {
      text: 'Navigation.',
      image: 'img/2dcontent/330E/14.3.2.9.jpg',
    },
    '14_3_2_10': {
      text: 'Connected Music.',
      image: 'img/2dcontent/330E/14.3.2.10.jpg',
    },
    '14_3_2_11': {
      text: 'Efficiency and Emissions.',
      image: 'img/2dcontent/330E/14.3.2.11.jpg',
    },

    // X5
    '14_3_3_2': {
      text: 'Lighting.',
      image: 'img/2dcontent/X5/14.3.3.2.jpg',
    },
    '14_3_3_3': {
      text: 'Parking assistant.',
      image: 'img/2dcontent/X5/14.3.3.3.jpg',
    },
    '14_3_3_4': {
      text: 'Charging Point.',
      image: 'img/2dcontent/X5/14.3.3.4.jpg',
    },
    '14_3_3_5': {
      text: 'Intelligent Personal Assistant.',
      image: 'img/2dcontent/X5/14.3.3.5.jpg',
    },
    '14_3_3_6': {
      text: 'Head-Up Display.',
      image: 'img/2dcontent/X5/14.3.3.6.jpg',
    },
    '14_3_3_7': {
      text: 'Gesture Control.',
      image: 'img/2dcontent/X5/14.3.3.7.jpg',
    },
    '14_3_3_8': {
      text: 'Sound System.',
      image: 'img/2dcontent/X5/14.3.3.8.jpg',
    },
    '14_3_3_9': {
      text: 'Navigation.',
      image: 'img/2dcontent/X5/14.3.3.9.jpg',
    },
    '14_3_3_10': {
      text: 'Connected Music.',
      image: 'img/2dcontent/X5/14.3.3.10.jpg',
    },
    '14_3_3_11': {
      text: 'Efficiency and Emissions.',
      image: 'img/2dcontent/X5/14.3.3.11.jpg',
    },
  },

  rangeValues: {
    'X1': {
      options: [17, 25, 32],
      correctOption: 32,
    },
    '330E': {
      options: [18, 25, 35],
      correctOption: 35,
    },
    'X5': {
      options: [19, 38, 54],
      correctOption: 54,
    },
  },

  savings2D: {
    image: 'img/2dcontent/2/1.png',
    text: 'Value.',
  },

  technology2D: {
    image: 'img/2dcontent/5/1.png',
    text: 'Intelligent Personal Assistant',
  },

  /* Tutorial */
  tutorialSteps: [{
    text: 'Tap to place your BMW',
    event: '4_1',
    img: 'img/tutorial/guide_place.svg',
    analyticsObject: {
      category: 'FIRST STEPS',
      action: 'Click',
      event: 'Place Model',
      label: 'Place',
    },
  }, {
    text: 'Pinch to scale',
    event: '4_4',
    img: 'img/tutorial/guide_scale.svg',
    analyticsObject: {
      category: 'FIRST STEPS',
      action: 'Scale',
      event: 'Scale Model',
      label: 'Scale',
    },
  }, {
    text: 'Swipe to move',
    event: '4_2',
    img: 'img/tutorial/guide_move.svg',
    analyticsObject: {
      category: 'FIRST STEPS',
      action: 'Drag',
      event: 'Move Model',
      label: 'Move',
    },
  }, {
    text: 'Twist to rotate',
    event: '4_3',
    img: 'img/tutorial/guide_rotate.svg',
    analyticsObject: {
      category: 'FIRST STEPS',
      action: 'Rotate',
      event: 'Rotate Model',
      label: 'Rotate',
    },
  }],

  faqs: [
    {
      question: 'What is a PHEV?',
      answer: `
        PHEV stands for plug-in hybrid electric vehicle - a car that has both a rechargeable electric motor and a combustion engine powered by petrol.
      `,
    },
    {
      question: 'How does a plug-in hybrid work?',
      answer: `
        On short journeys at low speeds, a plug-in hybrid generally runs on pure electric power. For maximum efficiency on longer journeys, at higher speeds or when rapid acceleration is needed, the car will automatically switch to the combustion engine.
        <br /><br />
        You can find out more <a href="https://www.testyourdrive.co.uk/phev?tl=grp-vivi-vivi-pro-mn-.-.-howphevw-.-." target="_blank">here</a>.
      `,
    },
    {
      question: 'What is the (electric) range of a plug-in hybrid?',
      answer: `
        The electric-only range of a BMW plug-in hybrid is between 26 and 54 miles*, depending on the model. When used in combination with the combustion engine, this range is significantly higher.
        <br /><br />
        <span class="notes">*Range figures are provided for comparability purposes. They may not reflect real life driving results, which will depend upon a number of factors including accessories fitted (post-registration), variations in weather, driving styles and vehicle load. The vehicle needs to be fully recharged.</span>
      `,
    },
    {
      question: 'How is a plug-in hybrid vehicle charged?',
      answer: `
        A plug-in hybrid can be charged at any of the thousands of charging stations throughout the country, or at home, using a household plug socket.
        <br /><br />
        To find out more, click <a href="https://www.bmw.co.uk/en/topics/discover/electric-and-phev/plug-in-hybrids.html?tl=grp-vivi-vivi-pro-mn-.-.-howcharg-.-." target="_blank">here</a>.
      `,
    },
    {
      question: 'Can a plug-in hybrid be charged during its journey?',
      answer: `
        Using energy created by braking, the battery’s charge level will be ‘topped up’ during your journey. However to fully charge the vehicle, it will need to be plugged in.
        <br /><br />
        You can find out more <a href="https://www.bmw.co.uk/en/topics/discover/electric-and-phev/plug-in-hybrids.html?tl=grp-vivi-vivi-pro-mn-.-.-phevjour-.-." target="_blank">here</a>.
      `,
    },
    {
      question: 'How do the ranges differ between plug-in hybrid and electric vehicles?',
      answer: `
        An electric vehicle such as the BMW i3 runs on electric power only and has a range of approximately 188 miles*. As a plug-in hybrid combines an electric motor and a combustion engine, its range is greater.
        <br /><br />
        <span class="notes">*These figures may not reflect real life driving results, which will depend upon a number of factors including the starting charge of the battery, accessories fitted (post-registration), variations in weather, driving styles and vehicle load. Figures shown are for comparability purposes. Only compare fuel consumption, CO2 and electric range figures with other cars tested to the same technical procedures. Plug in hybrid vehicles require mains electricity for charging. These are provisional values.</span>
      `,
    },
    {
      question: 'Do I have to select petrol or electric mode?',
      answer: `
        Providing the car has been sufficiently charged, you can select petrol, electric or Anticipatory Hybrid Mode, which will select either power source according to the environment, speed and journey.
        <br /><br />
        You can find out more <a href="https://www.bmw.co.uk/en/topics/discover/electric-and-phev/plug-in-hybrids.html?tl=grp-vivi-vivi-pro-mn-.-.-phevmode-.-." target="_blank">here</a>.
      `,
    },
    {
      question: 'Will a plug-in hybrid save me money?',
      answer: `
        As electricity is generally cheaper than petrol, running a plug-in hybrid vehicle can save money over a petrol or diesel-only car, depending on your regular journey. And driving conditions. There are also many cities that offer reduced charges for low-emission vehicles such as plug-in hybrids.
        <br /><br />
        Find out more about how much your potential fuel savings <a href="https://www.testyourdrive.co.uk/?tl=grp-vivi-vivi-pro-mn-.-.-phevmone-.-." target="_blank">here</a>.
      `,
    },
    {
      question: 'Is performance compromised?',
      answer: `
        Absolutely not - after all, this is a BMW. A plug-in hybrid offers excellent performance and efficiency, so you don’t have to choose between petrol and electric.
      `,
    },
    {
      question: 'How many PHEVs does BMW have in the range?',
      answer: `
        There are a large range of PHEVs, from the 2 Series to the 7 Series, as well as a number of X models. More are always being added, and you can explore the full range at <a href="https://www.bmw.co.uk/en/all-models/phev.html?tl=grp-vivi-vivi-pro-mn-.-.-phevquan-.-." target="_blank">bmw.co.uk</a>.
      `,
    },
    {
      question: 'Need more help?',
      answer: `
        If you need more help with a question not included above, visit our <a href="https://discover.bmw.co.uk/help/?tl=grp-vivi-vivi-pro-mn-.-.-morehelp-.-." target="_blank">Online Genius Service</a>.
      `,
    },
  ],

  support: [
    {
      question: 'I can’t hear the chatbot.',
      answer: `
        Make sure the "mute chatbot" button in the top right is not selected.
        <br />
        Please also make sure the volume on your phone is on.
      `,
    },
    {
      question: 'The chatbot doesn’t seem to hear me.',
      answer: `
        Remember to press and hold the blue button at the bottom of the screen, then speak clearly into your phone’s microphone.
      `,
    },
    {
      question: 'I don’t want to hear the chatbot.',
      answer: `
        To mute the chatbot, press the "mute" button in the top right of your screen. Subtitles will then take you through the virtual viewer.
      `,
    },
    {
      question: 'What can I ask the chatbot?',
      answer: `
        You can ask questions about plug-in hybrids in general, or the specific model you are looking at:
        <ul>
          <li>What is a plug-in hybrid?</li>
          <li>Tell me about the BMW power modes?</li>
          <li>What are connected services?</li>
          <li>How does the parking assistant work?</li>
        </ul>

        You can interact with the car by saying things such as:
        <ul>
          <li>Open the doors.</li>
          <li>Turn on the radio.</li>
          <li>Turn on the headlights.</li>
          <li>Change the upholstery.</li>
        </ul>

        You can even make small talk with the chatbot. Give it a try, see what it says!
      `,
    },
    {
      question: 'What is augmented reality?',
      answer: `
        An augmented reality experience shows virtual objects in the real world through a screen. In this case, you’ll be able to see a virtual BMW plug-in hybrid wherever you are.
      `,
    },
    {
      question: 'I can’t see the BMW.',
      answer: `
        If you are just starting the experience, the tutorial will help you place the virtual BMW in front of you. Tap the screen to place the car in the environment.
        <br /><br />
        If you had the model on the screen and you moved to a new space, the model will automatically be replaced in the center of your screen within a few seconds.
        <br /><br />
        If you still have problems, try resetting the app by clicking on the BMW logo  on the top left corner of the screen.
      `,
    },
    {
      question: 'How can I explore the BMW?',
      answer: `
        Physically move your phone in the environment. You can walk around the car, get close to look at details and even get inside.
      `,
    },
    {
      question: 'How can I see the interior of the BMW?',
      answer: `
        You can physically move your phone into the car, or you can ask the bot to open the door for you. To enable you to see more details, we recommend you make the car large before you get inside.
      `,
    },
    {
      question: 'Can I see the BMW in real size?',
      answer: `
        By default, the car will not be in real-size. You can press the calibrate button on the bottom left of the screen to scale the car to the correct size.
      `,
    },
  ],
});
