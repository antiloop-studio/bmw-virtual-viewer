export default class TimerService {
  step = 0;

  stepDelay = 1000;

  isPaused = false;

  intervalId = null;

  constructor(startStep = 0) {
    this.step = startStep;
    this.events = {
      change: () => { },
    };
  }

  start() {
    this.intervalId = setInterval(() => {
      if (this.isPaused) {
        return;
      }

      this.step += 1;
      this.events.change(this.step);
    }, this.stepDelay);
  }

  pause() {
    this.isPaused = true;
  }

  resume() {
    this.isPaused = false;
  }

  stop() {
    clearInterval(this.intervalId);
    this.intervalId = null;
  }

  on(listener, fn) {
    this.events[listener] = fn;
  }
}
