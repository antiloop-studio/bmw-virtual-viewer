import { intentEventsGroupedByModelCode } from '@/constants';
import TimerService from './TimerService';

const Timer = new TimerService();

const userJourneySteps = [
  {
    name: 'welcome',
    seen: false,
    delay: 1,
    playWithoutMinimumDelayBetweenSteps: true,
    dispatchs: [
      {
        event: 'JourneyState/launchModelDependentEvent',
        value: 'welcome',
      },
    ],
  },
  {
    name: 'feelFreeToAskAQuestion',
    seen: false,
    delay: 1,
    dispatchs: [
      {
        event: 'launchEvent',
        value: '0_1',
      },
    ],
  },
  {
    name: 'openDoorsOrWindows',
    seen: false,
    delay: 10,
    dispatchs: [
      {
        event: 'launchEvent',
        value: '0_5_1',
      },
    ],
  },
  {
    name: 'turnOnRadioPossibility',
    seen: false,
    delay: 20,
    dispatchs: [
      {
        event: 'JourneyState/radioPossibilityOrDecreasePersonalisationDelay',
      },
    ],
  },
  {
    name: 'BottomMenuPersonalization',
    seen: false,
    delay: 10,
    dispatchs: [
      {
        event: 'setPersoMenuJourney',
        value: true,
      },
      {
        event: 'setComponentContent',
        value: {
          component: 'BottomMenuComponent',
          content: 'BottomMenuPersonalization',
        },
      },
      {
        event: 'enableComponent',
        value: 'BottomMenuComponent',
      },
    ],
  },
  {
    name: 'takephoto',
    seen: false,
    delay: 5,
    dispatchs: [
      {
        event: 'launchEvent',
        value: '0_4_1',
      },
    ],
  },
  {
    name: 'BottomMenuCharging',
    seen: false,
    delay: 15,
    dispatchs: [
      {
        event: 'setComponentContent',
        value: {
          component: 'BottomMenuComponent',
          content: 'BottomMenuCharging',
        },
      },
      {
        event: 'enableComponent',
        value: 'BottomMenuComponent',
      },
      {
        event: 'JourneyState/launchModelDependentEvent',
        value: 'chargingStarter',
      },
    ],
  },
  {
    name: 'BottomMenuSavings',
    seen: false,
    delay: 10,
    dispatchs: [
      {
        event: 'setSavingsNeedsConfirmation',
        value: true,
      },
      {
        event: 'setComponentContent',
        value: {
          component: 'BottomMenuComponent',
          content: 'BottomMenuSavings',
        },
      },
      {
        event: 'enableComponent',
        value: 'BottomMenuComponent',
      },
      {
        event: 'JourneyState/launchModelDependentEvent',
        value: 'savingsStarter',
      },
    ],
  },
  {
    name: 'RegisterInterest',
    seen: false,
    delay: 2,
    dispatchs: [{
      event: 'launchEvent',
      value: '0_13',
    }],
  },
  {
    name: 'BottomMenuXRay',
    seen: false,
    delay: 10,
    dispatchs: [
      {
        event: 'setXRayNeedsConfirmation',
        value: true,
      },
      {
        event: 'setComponentContent',
        value: {
          component: 'BottomMenuComponent',
          content: 'BottomMenuXRay',
        },
      },
      {
        event: 'enableComponent',
        value: 'BottomMenuComponent',
      },
      {
        event: 'JourneyState/launchModelDependentEvent',
        value: 'powerModesStarter',
      },
    ],
  },
  {
    name: 'BottomMenuRange',
    seen: false,
    delay: 5,
    dispatchs: [
      {
        event: 'setComponentContent',
        value: {
          component: 'BottomMenuComponent',
          content: 'BottomMenuRange',
        },
      },
      {
        event: 'enableComponent',
        value: 'BottomMenuComponent',
      },
      {
        event: 'JourneyState/launchModelDependentEvent',
        value: 'rangeStarter',
      },
    ],
  },
  {
    name: 'bmwTechnology',
    seen: false,
    delay: 10,
    dispatchs: [
      {
        event: 'launchEvent',
        value: '18_1',
      },
    ],
  },
  {
    name: 'promoteRealScaleFunctionality',
    seen: false,
    delay: 10,
    dispatchs: [
      {
        event: 'launchEvent',
        value: '4_5',
      },
    ],
  },
];

export default {
  namespaced: true,
  state: {
    hasJourneyStarted: false,
    currentStep: undefined,
    steps: userJourneySteps,
    resumeJourneyTimeout: null,
    minimumDelayBetweenSteps: 3,
    alreadyShowedPersonalisationConclusion: false,
    alreadySuggestedOtherPersonalisation: false,
    alreadyPlayedTechnologyIntro: false,
  },

  mutations: {
    setHasJourneyStarted(state, hasStarted) {
      state.hasJourneyStarted = hasStarted;
    },
    setCurrentStep(state, stepName) {
      state.currentStep = stepName;
    },
    markStepAsSeen(state, stepName) {
      const step = state.steps.find((s) => s.name === stepName);
      if (step) {
        step.seen = true;
      }
    },
    setAlreadyShowedPersonalisationConclusion(state, hasShown) {
      state.alreadyShowedPersonalisationConclusion = hasShown;
    },
    setAlreadySuggestedOtherPersonalisation(state, hasSuggested) {
      state.alreadySuggestedOtherPersonalisation = hasSuggested;
    },
    setAlreadyPlayedTechnologyIntro(state, hasPlayed) {
      state.alreadyPlayedTechnologyIntro = hasPlayed;
    },
  },

  getters: {
    isShowingJourneyPausingComponent(_, __, rootState) {
      const journeyPausingComponents = [
        'ExternalLink',
        'BottomMenuComponent',
        'MainMenuComponent',
        'InfoPanel',
        'ModalComponent',
        'TakePhoto',
      ];

      const { currentComponents } = rootState.ViewsState;
      const isShowingJourneyPausingComponent = currentComponents
        .some((c) => journeyPausingComponents.includes(c));

      return isShowingJourneyPausingComponent;
    },

    shouldJourneyBePaused(state, getters, rootState, rootGetters) {
      const {
        getIsRecording,
        isRecordingOrRecentlyFinishedRecording,
        isWaitingForServerResponse,
      } = rootGetters;
      const { isShowingJourneyPausingComponent } = getters;
      const isAnimationPlaying = rootGetters['WebGLState/isAnimationPlaying'];
      const isHDModelLoaded = rootGetters['WebGLState/isHDModelLoaded'];
      const isAudioPlaying = rootGetters['PlayerState/getIsPlaying'];
      const cannotPlayAudio = !rootGetters['PlayerState/canPlay'];
      const isConfiguringRealScale = rootGetters['WebGLState/getIsConfiguringRealScale'];

      return isShowingJourneyPausingComponent
        || isAudioPlaying
        || getIsRecording
        || isRecordingOrRecentlyFinishedRecording
        || isAnimationPlaying
        || cannotPlayAudio
        || isConfiguringRealScale
        || isWaitingForServerResponse
        || !isHDModelLoaded;
    },

    upcomingStep(state) {
      return state.steps.find((step) => !step.seen);
    },

    accumulatedDelay(state) {
      return state.steps
        .filter((step) => step.seen)
        .reduce((acc, step) => acc + step.delay, 0);
    },
  },

  actions: {
    startJourney({ dispatch, commit }) {
      Timer.on('change', (time) => {
        dispatch('handleTimerTick', time);
      });
      Timer.start();
      commit('setHasJourneyStarted', true);
    },

    handleTimerTick({ commit, dispatch, getters }, time) {
      const { upcomingStep } = getters;

      if (!upcomingStep) {
        Timer.stop();
        commit('setCurrentStep', undefined);
        return;
      }

      if ((getters.accumulatedDelay + upcomingStep.delay) === time) {
        dispatch('startNextStep');
      }
    },

    pauseTimer() {
      clearTimeout(this.resumeJourneyTimeout);
      this.resumeJourneyTimeout = null;
      Timer.pause();
    },

    resumeTimer({ state, getters }) {
      const { upcomingStep } = getters;

      if (!upcomingStep) {
        Timer.resume();
        return;
      }

      const passedSeconds = Timer.step;
      const secondsTillNextStep = (getters.accumulatedDelay + upcomingStep.delay) - passedSeconds;

      if (secondsTillNextStep > state.minimumDelayBetweenSteps
        || upcomingStep.playWithoutMinimumDelayBetweenSteps) {
        Timer.resume();
        return;
      }

      const nextStepDelay = state.minimumDelayBetweenSteps - secondsTillNextStep;
      this.resumeJourneyTimeout = setTimeout(() => {
        clearTimeout(this.resumeJourneyTimeout);
        this.resumeJourneyTimeout = null;
        Timer.resume();
      }, nextStepDelay * 1000);
    },

    startNextStep({ commit, dispatch, getters }) {
      const { upcomingStep } = getters;
      commit('setCurrentStep', upcomingStep.name);
      commit('markStepAsSeen', upcomingStep.name);
      upcomingStep.dispatchs.forEach((d) => {
        dispatch(d.event, d?.value, { root: true });
      });
    },

    markStepAsSeen({ commit }, name) {
      commit('markStepAsSeen', name);
    },

    suggestOtherPersonalisation({ dispatch, rootGetters }) {
      const isInsideCar = rootGetters['WebGLState/isInsideCar'];
      if (isInsideCar) {
        dispatch('launchEvent', '0_2', { root: true });
      } else {
        dispatch('launchEvent', '0_3', { root: true });
      }
    },

    launchModelDependentEvent({ dispatch, rootGetters }, eventName) {
      const currentModelCode = rootGetters.getSelectedModel.code;

      const modelSpecificIntent = intentEventsGroupedByModelCode[currentModelCode][eventName];
      if (modelSpecificIntent) {
        dispatch('launchEvent', modelSpecificIntent, { root: true });
      }
    },

    radioPossibilityOrDecreasePersonalisationDelay({ state, dispatch, rootGetters }) {
      const isInsideCar = rootGetters['WebGLState/isInsideCar'];
      if (isInsideCar) {
        const visitedIntents = rootGetters.getIntents ?? [];
        const radioWasNotTurnedOnYet = !visitedIntents.includes('15.1.5');
        const soundSystemHotspotWasNotTriggeredYet = !(visitedIntents.includes('14.3.1.8')
        || visitedIntents.includes('14.3.2.8')
        || visitedIntents.includes('14.3.3.8'));
        if (radioWasNotTurnedOnYet && soundSystemHotspotWasNotTriggeredYet) {
          dispatch('launchEvent', '0_5_2', { root: true });
        }
      } else {
        const personalisationStep = state.steps.find((step) => step.name === 'BottomMenuPersonalization');
        if (!personalisationStep.seen) {
          personalisationStep.delay = 3;
        }
      }
    },

    technologyStartTrigger({ state, dispatch, commit }) {
      if (!state.alreadyPlayedTechnologyIntro) {
        dispatch('launchModelDependentEvent', 'technologyStarter');
        commit('setAlreadyPlayedTechnologyIntro', true);
      } else {
        dispatch('launchEvent', '18_1_4', { root: true });
      }
    },
  },
};
