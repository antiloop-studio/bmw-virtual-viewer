import {
  views,
  legalTerms,
  bmwTechnologyIntroIntents,
  bmwTechnologyIntents,
  bmwTechnologies,
  intents,
  rangeValues,
  externalLinkDuringIntent,
  externalLinkAfterIntent,
  hotspots,
  animations,
  generalHotspotIntentToKeywordMap,
} from '@/constants';
import Bus from '@/store/bus/Bus';
import Vue from 'vue';
/**
 * Views state
 */
export default {
  state: {
    views,
    currentView: undefined,
    seenViews: [],
    currentComponents: [],
    seenContents: [],
    currentHotspotId: undefined,
    ModalComponent: '',
    BottomMenuComponent: '',
    LegalTermsComponent: '',
    ExternalLink: '',
    InfoPanel: '',
    logoClass: 'black',
    selectedModel: null,
    xRayNeedsConfirmation: false,
    savingsNeedsConfirmation: false,
    PersoMenuJourney: false,
    isComponentVisible: {},
    windowHasFocus: false,
    confirmAction: null,
  },
  mutations: {
    setView(state, findBy) {
      // Set view
      state.currentView = state.views.find(findBy);
      // Mark view as seen
      state.seenViews.push(state.currentView.name);
    },
    enableComponent(state, params) {
      // current components
      const idx = state.currentComponents.indexOf(params.component);
      if (params.setActive) {
        if (idx === -1) {
          state.currentComponents.push(params.component);
        }
      } else if (idx > -1) {
        state.currentComponents.splice(idx, 1);
      }
    },
    seeContent(state, name) {
      // seen components (and subcomponents)
      const idx = state.seenContents.indexOf(name);
      if (idx === -1) {
        state.seenContents.push(name);
      }
    },
    setComponentContent(state, params) {
      state[params.component] = params.content;
    },
    setCurrentHotspotId(state, id) {
      state.currentHotspotId = id;
    },
    setSelectedModel(state, model) {
      state.selectedModel = model;
    },
    setLogoClass(state, color) {
      state.logoClass = color;
    },
    setXRayNeedsConfirmation(state, status) {
      state.xRayNeedsConfirmation = status;
    },
    setSavingsNeedsConfirmation(state, status) {
      state.savingsNeedsConfirmation = status;
    },
    setPersoMenuJourney(state, status) {
      state.PersoMenuJourney = status;
    },
    storeComponentVisibleState(state, { componentName, isVisible }) {
      Vue.set(state.isComponentVisible, componentName, isVisible);
    },
    setWindowHasFocus(state, hasFocus) {
      state.windowHasFocus = hasFocus;
    },
    setConfirmAction(state, confirmAction) {
      state.confirmAction = confirmAction;
    },
  },
  actions: {
    setStartView() {
      this.commit('setView', (v) => v.name === 'welcome');
    },
    setViewByName(context, name) {
      context.commit('setView', (v) => v.name === name);
    },
    enableComponent(context, component) {
      context.commit('enableComponent', { setActive: true, component });
      context.commit('storeComponentVisibleState', {
        componentName: component,
        isVisible: true,
      });
    },
    disableComponent(context, component) {
      context.commit('enableComponent', { setActive: false, component });
      context.commit('storeComponentVisibleState', {
        componentName: component,
        isVisible: false,
      });
    },
    seeContent(context, name) {
      context.commit('seeContent', name);
    },
    setComponentContent(context, params) {
      context.commit('setComponentContent', params);
    },
    setCurrentHotspotId(context, id) {
      context.commit('setCurrentHotspotId', id);
    },
    setSelectedModel(context, model) {
      context.commit('setSelectedModel', model);
    },
    setLogoClass(context, color) {
      context.commit('setLogoClass', color);
    },
    setXRayNeedsConfirmation(context, status) {
      context.commit('setXRayNeedsConfirmation', status);
    },
    setSavingsNeedsConfirmation(context, status) {
      context.commit('setSavingsNeedsConfirmation', status);
    },
    setPersoMenuJourney(context, status) {
      context.commit('setPersoMenuJourney', status);
    },
    stopCurrentInteraction({ dispatch }) {
      dispatch('stopCurrentInteractionLocally');
      dispatch('launchEvent', '0_0', { root: true });
    },
    stopCurrentInteractionLocally({ dispatch }) {
      dispatch('WebGLState/closeCurrentHotspot');
      Bus.$emit('inactiveBottomMenuComponent');
      dispatch('disableComponent', 'InfoPanel');
      dispatch('PlayerState/stop', null, { root: true });
      dispatch('disableComponent', 'LegalTermsComponent');
      dispatch('setComponentContent', {
        component: 'LegalTermsComponent',
        content: null,
      });
      dispatch('WebGLState/stopAnimation');
    },
    setWindowHasFocus({ commit }, hasFocus) {
      commit('setWindowHasFocus', hasFocus);
    },
    setConfirmAction({ commit }, confirmAction) {
      commit('setConfirmAction', confirmAction);
    },
  },
  getters: {
    getCurrentView(state) { return state.currentView; },
    getCurrentComponents(state) { return state.currentComponents; },
    getSeenContents(state) { return state.seenContents; },
    getCurrentHotspotId(state) { return state.currentHotspotId; },
    getSeenViews(state) { return state.seenViews; },
    getModalComponent(state) { return state.ModalComponent; },
    getBottomMenuComponent(state) { return state.BottomMenuComponent; },
    getExternalLink(state) { return state.ExternalLink; },
    getSelectedModel(state) { return state.selectedModel; },
    getLogoClass(state) { return state.logoClass; },
    getXRayNeedsConfirmation(state) { return state.xRayNeedsConfirmation; },
    getSavingsNeedsConfirmation(state) { return state.savingsNeedsConfirmation; },
    getPersoMenuJourney(state) { return state.PersoMenuJourney; },
    getWindowHasFocus(state) { return state.windowHasFocus; },
    getConfirmAction(state) { return state.confirmAction; },
    waitingForResponseLoadingIndicatorVisible(state, getters, rootState, rootGetters) {
      const { isWaitingForUserTriggeredActionResponse } = rootGetters;
      const { isHDModelLoaded } = rootState.WebGLState;

      return isHDModelLoaded && isWaitingForUserTriggeredActionResponse;
    },

    currentlyVisibleComponents(state, getters, rootState) {
      const componentMap = rootState.ViewsState.isComponentVisible;
      const visibleComponentNames = Object.keys(componentMap)
        .filter((componentName) => componentMap[componentName]);

      return visibleComponentNames;
    },

    shouldHotspotsBeVisible(state, getters, rootState, rootGetters) {
      const { isHDModelLoaded } = rootState.WebGLState;

      const { currentlyVisibleComponents } = getters;
      const isAnimationPlaying = rootGetters['WebGLState/isAnimationPlaying'];
      const isConfiguringRealScale = rootGetters['WebGLState/getIsConfiguringRealScale'];

      const hotspotBlockingComponents = [
        'BottomMenuComponent',
        'InfoPanel',
        'TakePhoto',
      ];

      const blockedByComponent = hotspotBlockingComponents
        .some((compName) => currentlyVisibleComponents.includes(compName));

      return isHDModelLoaded
        && !(blockedByComponent || isAnimationPlaying || isConfiguringRealScale);
    },

    shouldTriggerHotspotByKeyword(state, getters) {
      const lastIntent = getters.getLastIntent;
      if (Object.keys(generalHotspotIntentToKeywordMap).includes(lastIntent)) {
        return generalHotspotIntentToKeywordMap[lastIntent];
      }

      return null;
    },

    shouldShowHotspotInfoPanel(state, getters) {
      const lastIntent = getters.getLastIntent;
      if (!lastIntent) {
        return null;
      }

      const lastIntentEvent = lastIntent.replace(/\./gi, '_');
      const hasInfoPanelForEvent = Object.keys(hotspots).includes(lastIntentEvent);
      if (!hasInfoPanelForEvent) {
        return null;
      }

      return {
        pages: [hotspots[lastIntentEvent]],
        onDismiss: () => {
          this.$store.dispatch('PlayerState/stop');
          this.$store.dispatch('WebGLState/closeCurrentHotspot');
        },
      };
    },

    closeButtonVisibleOnBottomMenu(state, getters) {
      const showingSavingsConfirmPanel = getters.getBottomMenuComponent === 'BottomMenuSavings'
        && state.savingsNeedsConfirmation;

      const showingXRayConfirmPanel = getters.getBottomMenuComponent === 'BottomMenuXRay'
        && state.xRayNeedsConfirmation;

      const visibleOnBottomPanel = ['BottomMenuRange', 'BottomMenuCharging', 'BottomMenuPersonalization']
        .includes(getters.getBottomMenuComponent);

      return visibleOnBottomPanel || showingSavingsConfirmPanel || showingXRayConfirmPanel;
    },

    shouldPlayAnimationId(state, getters, rootState) {
      const lastIntent = getters.getLastIntent;
      const hasAudioToPlay = !!rootState.PlayerState.currentAudio;
      const animationConfig = animations
        .find((config) => config.triggeredByIntent.includes(lastIntent));

      if (hasAudioToPlay && animationConfig) {
        return animationConfig.animationId;
      }

      return null;
    },

    playingCloseableContent(state, getters, rootState, rootGetters) {
      const isBottomMenuComponentVisible = getters.currentlyVisibleComponents.includes('BottomMenuComponent');
      if (isBottomMenuComponentVisible) {
        return false;
      }

      const isAnimationPlaying = rootGetters['WebGLState/isAnimationPlaying'];
      const { lastIntent } = rootState.ApiState;
      const hasAudioToPlay = !!rootState.PlayerState.currentAudio;

      const isPlayingChargingDescriptionAudio = hasAudioToPlay
        && intents.charging.explanations.includes(lastIntent);
      const isPlayingRadio = hasAudioToPlay && lastIntent === '15.1.5';

      return isAnimationPlaying || isPlayingChargingDescriptionAudio || isPlayingRadio;
    },

    shouldShowExternalLink(state, getters, rootState, rootGetters) {
      const { isWaitingForServerResponse } = rootGetters;
      const { currentAudio, lastAudioGotStopped } = rootState.PlayerState;

      if (currentAudio) {
        if (Object.keys(externalLinkDuringIntent).includes(getters.getLastIntent)) {
          return externalLinkDuringIntent[getters.getLastIntent];
        }
      } else if (!lastAudioGotStopped && !isWaitingForServerResponse
        && Object.keys(externalLinkAfterIntent).includes(getters.getLastIntent)) {
        return externalLinkAfterIntent[getters.getLastIntent];
      }

      return null;
    },

    shouldShowTakePhotoComponent(state, getters) {
      return getters.getLastIntent === '15.2.1';
    },

    shouldTriggerXRayStarter(state, getters) {
      return getters.getLastIntent === intents.powerModes.trigger;
    },

    shouldShowXRayConfirmationDialog(state, getters) {
      return intents.powerModes.starters.includes(getters.getLastIntent);
    },

    shouldTriggerRangeStarter(state, getters) {
      return getters.getLastIntent === intents.range.trigger;
    },

    rangeAnswerOptions(state, getters, rootState, rootGetters) {
      const selectedModelCode = rootGetters.getSelectedModel.code;

      return rangeValues[selectedModelCode].options;
    },

    correctRangeAnswer(state, getters, rootState, rootGetters) {
      const selectedModelCode = rootGetters.getSelectedModel.code;

      return rangeValues[selectedModelCode].correctOption;
    },

    shouldShowRangeQuiz(state, getters) {
      return intents.range.starters.includes(getters.getLastIntent);
    },

    shouldShowChargingQuiz(state, getters) {
      return intents.charging.starters.includes(getters.getLastIntent);
    },

    shouldTriggerSavingsStarter(state, getters) {
      return getters.getLastIntent === '11.1';
    },

    shouldShowSavingsConfirmationDialog(state, getters) {
      return intents.savings.starters.includes(getters.getLastIntent);
    },

    shouldShowPersonalisationMenu(state, getters) {
      return getters.getLastIntent === '15.1.11';
    },
    shouldShowRealScaleConfirmationDialog(state, getters) {
      return getters.getLastIntent === '13.1';
    },

    firstUnplayedTechnology(state, getters, rootState, rootGetters) {
      const { seenTechnologyIntents } = rootState.ApiState;
      const currentModelCode = rootGetters.getSelectedModel.code;

      const firstUnseenIntent = bmwTechnologyIntents[currentModelCode]
        .find((intent) => !seenTechnologyIntents[currentModelCode].includes(intent));
      const firstUnseenIntentEvent = firstUnseenIntent.replace(/\./gi, '_');

      return bmwTechnologies.find((technology) => technology.intent === firstUnseenIntentEvent);
    },

    shouldShowTechnologyConfirmationDialog(state, getters) {
      return getters.getLastIntent === '18.1';
    },

    currentlyPlayingTechnology(state, getters) {
      const lastIntent = getters.getLastIntent;
      const technologyIntents = [...bmwTechnologyIntents.X1, ...bmwTechnologyIntents['330E'], ...bmwTechnologyIntents.X5];
      const lastIntentIsTechnology = !!technologyIntents.includes(lastIntent);

      if (!lastIntentIsTechnology) {
        return null;
      }

      const currentTechnologyIntentEvent = lastIntent.replace(/\./gi, '_');

      return bmwTechnologies
        .find((technology) => technology.intent === currentTechnologyIntentEvent);
    },

    currentTechologyProps(state, getters, rootState, rootGetters) {
      const lastIntent = getters.getLastIntent;
      const currentModelCode = rootGetters.getSelectedModel.code;
      const lastIntentIsTechnology = bmwTechnologyIntents[currentModelCode].includes(lastIntent);
      const lastIntentIsTechnologyIntro = bmwTechnologyIntroIntents.includes(lastIntent);

      const { lastAudioGotStopped } = rootState.PlayerState;
      let technologyIntroGotStopped;
      if (getters.getLastIntent === '18.1.4') {
        technologyIntroGotStopped = false;
      } else {
        technologyIntroGotStopped = lastAudioGotStopped;
      }

      const shouldShowTechnology = (lastIntentIsTechnologyIntro || lastIntentIsTechnology)
        && !technologyIntroGotStopped;

      if (shouldShowTechnology) {
        if (lastIntentIsTechnology) {
          return getters.currentlyPlayingTechnology;
        }
        return getters.firstUnplayedTechnology;
      }
      return null;
    },

    technologyIntentToLaunch(state, getters, rootState) {
      const hasAudioToPlay = !!rootState.PlayerState.currentAudio;
      const { lastAudioGotStopped } = rootState.PlayerState;

      let technologyIntroGotStopped;
      if (getters.getLastIntent === '18.1.4') {
        technologyIntroGotStopped = false;
      } else {
        technologyIntroGotStopped = lastAudioGotStopped;
      }
      const lastIntentTechnologyIntro = bmwTechnologyIntroIntents.includes(getters.getLastIntent);

      if (lastIntentTechnologyIntro && !technologyIntroGotStopped && !hasAudioToPlay) {
        return getters.firstUnplayedTechnology.intent;
      }

      return null;
    },

    shouldTriggerPriceExplanation(state, getters) {
      return getters.getLastIntent === '16.7';
    },

    legalTermsComponentContent(state, getters) {
      const matchedLegalTerm = legalTerms
        .find((terms) => terms.triggeringIntents.includes(getters.getLastIntent));

      if (matchedLegalTerm) {
        return { content: matchedLegalTerm.text, duration: matchedLegalTerm.duration };
      }

      return null;
    },

    shouldPlayRadio(state, getters) {
      return getters.getLastIntent === '15.1.5';
    },

    recordButtonVisible(state, getters, rootState, rootGetters) {
      const visitedIntents = rootGetters.getIntents ?? [];
      const notVisitedWelcomeIntent = !visitedIntents.includes('0.1');

      const { personalizationPanelVisible } = getters;
      const { experienceStarted, isHDModelLoaded } = rootState.WebGLState;
      const modelNotYetLoaded = experienceStarted && !isHDModelLoaded;

      const hasConfirmAction = !!rootState.ViewsState.confirmAction;
      const playingCloseableContent = rootGetters.playingCloseableContent ?? false;

      const { isMuted } = rootState.PlayerState;

      const isBottomMenuComponentVisible = getters.currentlyVisibleComponents.includes('BottomMenuComponent');
      const currentBottomMenuComponent = rootState.ViewsState.BottomMenuComponent;
      const quizVisible = isBottomMenuComponentVisible && ['BottomMenuRange',
        'BottomMenuCharging'].includes(currentBottomMenuComponent);

      const recordingForbidden = (notVisitedWelcomeIntent
        || modelNotYetLoaded
        || personalizationPanelVisible
        || isMuted
        || quizVisible);

      return !recordingForbidden || hasConfirmAction || playingCloseableContent;
    },

    quizPanelVisible(state, getters, rootState) {
      const isBottomMenuComponentVisible = getters.currentlyVisibleComponents.includes('BottomMenuComponent');
      const currentBottomMenuComponent = rootState.ViewsState.BottomMenuComponent;
      const quizBottomMenuComponentNames = [
        'BottomMenuXRay',
        'BottomMenuRange',
        'BottomMenuCharging',
        'BottomMenuSavings',
      ];

      const quizPanelVisible = isBottomMenuComponentVisible
        && quizBottomMenuComponentNames.includes(currentBottomMenuComponent);

      return quizPanelVisible;
    },

    personalizationPanelVisible(state, getters, rootState) {
      const isBottomMenuComponentVisible = getters.currentlyVisibleComponents.includes('BottomMenuComponent');
      const currentBottomMenuComponent = rootState.ViewsState.BottomMenuComponent;

      const isPersonalizationPanelVisible = isBottomMenuComponentVisible
        && currentBottomMenuComponent === 'BottomMenuPersonalization';

      return isPersonalizationPanelVisible;
    },

    bottomSideButtonsVisible(state, getters, rootState, rootGetters) {
      const isHDModelLoaded = rootGetters['WebGLState/isHDModelLoaded'];
      const isAnimationPlaying = rootGetters['WebGLState/isAnimationPlaying'];
      const isConfiguringRealScale = rootGetters['WebGLState/getIsConfiguringRealScale'];
      const { isWaitingForPowerModesExplanation } = rootGetters;
      const { quizPanelVisible, personalizationPanelVisible } = getters;

      const viewsWithoutBottomSideActions = [
        'PermissionsCameraComponent',
        'PermissionsDesktopComponent',
        'PermissionsDisabledComponent',
        'PermissionsMicroComponent',
        'PermissionsMotionComponent',
        'CarSelectionComponent',
        'HomeComponent',
      ];

      const currentViewComponentName = getters.getCurrentView.view;
      const inactiveOnCurrentView = viewsWithoutBottomSideActions
        .includes(currentViewComponentName);

      return isHDModelLoaded
        && !(isAnimationPlaying
          || inactiveOnCurrentView
          || isConfiguringRealScale
          || quizPanelVisible
          || personalizationPanelVisible
          || isWaitingForPowerModesExplanation);
    },

    scaleButtonVisible(state, getters) {
      return getters.bottomSideButtonsVisible;
    },

    settingsButtonVisible(state, getters) {
      return getters.bottomSideButtonsVisible;
    },

    mainMenuButtonVisible(state, getters, rootState, rootGetters) {
      const isHDModelLoaded = rootGetters['WebGLState/isHDModelLoaded'];
      const isConfiguringRealScale = rootGetters['WebGLState/getIsConfiguringRealScale'];

      return isHDModelLoaded && !isConfiguringRealScale;
    },

    muteMenuButtonVisible(state, getters, rootState, rootGetters) {
      const currentModelCode = rootGetters.getSelectedModel?.code;

      return currentModelCode !== undefined;
    },
  },
};
