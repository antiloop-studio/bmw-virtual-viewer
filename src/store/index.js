import Vue from 'vue';
import Vuex from 'vuex';
import MicrophoneState from './microphone/MicrophoneState';
import CameraState from './camera/CameraState';
import PlayerState from './player/PlayerState';
import ViewsState from './views/ViewsState';
import ApiState from './api/ApiState';
import WebGLState from './webgl/WebGLState';
import JourneyState from './journey/JourneyState';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    MicrophoneState,
    CameraState,
    PlayerState,
    ViewsState,
    ApiState,
    WebGLState,
    JourneyState,
  },
});
