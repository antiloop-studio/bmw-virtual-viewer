import axios from 'axios';

const uuid = require('uuid');

const session = uuid.v4();

export default class ApiService {
  static url(action) {
    let endpoint = process.env.VUE_APP_API_URL;

    switch (action) {
      case 'event': endpoint += '/event'; break;
      case 'text': endpoint += '/text'; break;
      case 'audio': endpoint += '/audio'; break;
      default: break;
    }
    return endpoint;
  }

  static sendEvent(event) {
    return axios({
      method: 'post',
      url: this.url('event'),
      data: {
        project: process.env.VUE_APP_DIALOGFLOW_PROJECT,
        session,
        event,
        locale: process.env.VUE_APP_DIALOGFLOW_LOCALE,
      },
    });
  }

  static sendText(text) {
    return axios({
      method: 'post',
      url: this.url('text'),
      data: {
        project: process.env.VUE_APP_DIALOGFLOW_PROJECT,
        session,
        text,
        locale: process.env.VUE_APP_DIALOGFLOW_LOCALE,
      },
    });
  }

  static sendAudio(blob) {
    const data = new FormData();
    data.set('session', session);
    data.append('file', blob, 'upload.wav');
    data.set('project', process.env.VUE_APP_DIALOGFLOW_PROJECT);
    data.set('locale', process.env.VUE_APP_DIALOGFLOW_LOCALE);

    return axios({
      method: 'post',
      url: this.url('audio'),
      data,
      headers: { 'Content-Type': 'multipart/form-data' },
    });
  }

  static intentIDRegex() {
    return /(([\w]|[\d])+\.)+(([\w]|[\d])+)/g;
  }
}
