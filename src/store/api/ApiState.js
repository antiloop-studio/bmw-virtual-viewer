import { relatedIntents, bmwTechnologyIntents, intents } from '@/constants';
import APIService from './ApiService';

const getResponseIntent = (queryResult) => {
  const displayName = queryResult
    && queryResult.intent
    && queryResult.intent.displayName;

  if (displayName) {
    const matches = displayName.match(APIService.intentIDRegex());
    if (matches && matches.length > 0) {
      return matches[0];
    }
  }

  return undefined;
};

export default {
  state: {
    lastRequest: undefined,
    lastResponse: undefined,
    lastIntent: undefined,
    lastSubtitles: undefined,
    requests: [],
    intents: [],
    seenTechnologyIntents: {
      'X1': [],
      '330E': [],
      'X5': [],
    },
    preloadedResponses: {},
    contexts: [],
    recognizedRecordings: [],
    requestsInProgress: 0,
    isWaitingForResponseToAudio: false,
    isWaitingForResponseToText: false,
    invalidStateObjects: [],
    lastIntentWasGenericFallback: false,
  },
  mutations: {
    storePreloadedResponse(state, { event, response }) {
      state.preloadedResponses[event] = response;
    },
    storeResponse(state, response) {
      const { subtitles, query_result: queryResult } = response.data;
      const outputContexts = queryResult && queryResult.outputContexts;

      state.lastResponse = response;
      state.lastSubtitles = subtitles;
      state.contexts = outputContexts || [];
    },
    storeIntent(state, intent) {
      state.lastIntent = intent;
      if (intent && !state.intents.includes(intent)) {
        state.intents.push(intent);
      }
    },
    storeTechnologyAsSeen(state, { model, intent }) {
      state.seenTechnologyIntents[model].push(intent);
    },
    clearSeenTechnologyIntents(state, model) {
      state.seenTechnologyIntents = {
        ...state.seenTechnologyIntents,
        [model]: [],
      };
    },
    storeLastAudioResponse(state, response) {
      const { query_result: queryResult } = response.data;

      if (queryResult?.queryText ?? false) {
        state.recognizedRecordings.unshift(queryResult?.queryText);
      }
    },
    updateLastRequest(state, request) {
      state.lastRequest = request;
      state.requests.push(request);
    },
    incrementInProgressRequestCount(state) {
      state.requestsInProgress += 1;
    },
    decrementInProgressRequestCount(state) {
      state.requestsInProgress -= 1;
    },
    setIsWaitingForResponseToAudio(state, isWaiting) {
      state.isWaitingForResponseToAudio = isWaiting;
    },
    setIsWaitingForResponseToText(state, isWaiting) {
      state.isWaitingForResponseToText = isWaiting;
    },
    storeStateBeforeInvalidEvent(state, invalidStateObject) {
      state.invalidStateObjects.push(invalidStateObject);
    },
    setLastIntentWasGenericFallback(state, value) {
      state.lastIntentWasGenericFallback = value;
    },
  },
  actions: {
    async preloadEvent({ commit }, event) {
      const response = await APIService.sendEvent(event);
      commit('storePreloadedResponse', { event, response });
    },

    async launchEvent({ state, commit, dispatch }, event) {
      commit('updateLastRequest', `event ${event}`);
      commit('incrementInProgressRequestCount');
      let response = state.preloadedResponses[event];
      if (!response || response.status !== 200) {
        response = await APIService.sendEvent(event);
      }
      commit('decrementInProgressRequestCount');
      dispatch('handleEventResponse', response);
    },

    async launchText({ commit, dispatch }, text) {
      commit('updateLastRequest', `text ${text}`);
      commit('incrementInProgressRequestCount');
      commit('setIsWaitingForResponseToText', true);
      const response = await APIService.sendText(text);
      commit('setIsWaitingForResponseToText', false);
      commit('decrementInProgressRequestCount');
      dispatch('handleEventResponse', response);
    },

    async launchAudio({ commit, dispatch }, blob) {
      if (blob.size < 500 || blob.size > 5000000) {
        return;
      }

      commit('updateLastRequest', `audio ${blob.size}`);
      commit('incrementInProgressRequestCount');
      commit('setIsWaitingForResponseToAudio', true);
      const response = await APIService.sendAudio(blob);
      commit('setIsWaitingForResponseToAudio', false);
      commit('decrementInProgressRequestCount');
      dispatch('handleEventResponse', response);
    },

    async handleTechnologyIntent({ commit, getters, rootGetters }, intent) {
      const currentModelCode = rootGetters.getSelectedModel.code;

      if (bmwTechnologyIntents[currentModelCode].includes(intent)) {
        commit('storeTechnologyAsSeen', { intent, model: currentModelCode });

        const { allTechnologiesHaveBeenStartedPlaying } = getters;
        if (allTechnologiesHaveBeenStartedPlaying) {
          commit('seeContent', 'BMWTechnology');
          commit('clearSeenTechnologyIntents', currentModelCode);
        }
      }
    },

    updateLastResponse({ commit, dispatch }, response) {
      const { query_result: queryResult } = response.data;
      const intent = getResponseIntent(queryResult);

      commit('storeResponse', response);

      commit('storeIntent', intent);
      dispatch('handleTechnologyIntent', intent);
    },

    handleEventResponse({ commit, dispatch }, response) {
      const { query_result: queryResult } = response.data;
      const intent = getResponseIntent(queryResult);

      dispatch('handleMissingOrGenericFallbackIntent', intent);
      dispatch('stopCurrentInteractionWhenNewIntentIsUnrelated', intent);
      dispatch('updateLastResponse', response);

      const isAudioResponse = queryResult?.speechRecognitionConfidence;
      if (isAudioResponse) {
        commit('storeLastAudioResponse', response);
      }

      if (intent) {
        dispatch('WebGLState/intentDetected', intent, { root: true });
      }
      if (response.data.audio_file_name) {
        dispatch(
          'PlayerState/play',
          `${process.env.VUE_APP_MEDIA_URL}/${response.data.audio_file_name}`,
          { root: true },
        );
      }
    },

    handleMissingOrGenericFallbackIntent({ state, commit, dispatch }, intent) {
      if (!intent || intent === '0.0.0') {
        if (!state.lastIntentWasGenericFallback) {
          dispatch('launchEvent', '0_0_1', { root: true });
        } else {
          dispatch('launchEvent', '0_0_2', { root: true });
        }
      }

      if (intent === '0.0.1') {
        commit('setLastIntentWasGenericFallback', true);
      } else {
        commit('setLastIntentWasGenericFallback', false);
      }

      if (intent === '0.0.0') {
        const invalidState = {
          lastIntentBeforeInvalidState: state.lastIntent,
          contextsBeforeInvalidState: state.contexts,
          lastRequest: state.lastRequest,
        };
        commit('storeStateBeforeInvalidEvent', invalidState);
      }
    },

    stopCurrentInteractionWhenNewIntentIsUnrelated({ state, dispatch }, newIntent) {
      const previousIntent = state.lastIntent;
      const previousIntentRelatives = relatedIntents.find((list) => list.includes(previousIntent));

      if (newIntent === '0.0' || newIntent === undefined || !previousIntentRelatives) {
        return;
      }

      const newIntentIsFromDifferentGroup = !previousIntentRelatives.includes(newIntent);

      if (newIntentIsFromDifferentGroup) {
        dispatch('stopCurrentInteractionLocally');
      }
    },

    analyticsEventHit(context, eventObject) {
      gtag('event', eventObject.action, {
        'event_category': eventObject.category,
        'event_label': eventObject.label,
        'value': eventObject.value,
      });
    },
  },
  getters: {
    getLastRequest(state) {
      return state.lastRequest;
    },
    getLastResponse(state) {
      return state.lastResponse;
    },
    getLastIntent(state) {
      return state.lastIntent;
    },
    getLastSubtitles(state) {
      return state.lastSubtitles;
    },
    getIntents(state) {
      return state.intents;
    },
    getContexts(state) {
      return state.contexts;
    },
    getRequests(state) {
      return state.requests;
    },
    isWaitingForServerResponse(state) {
      return state.requestsInProgress > 0;
    },
    isWaitingForPowerModesExplanation(state, getters, rootState) {
      const { isWaitingForServerResponse } = getters;
      const { lastRequest } = rootState.ApiState;
      const lastRequestedEventId = lastRequest?.startsWith('event ') && lastRequest.substr(6);
      const lastRequestIsPowerModesExplanationIntent = intents.powerModes.explanationIntentEvents
        .includes(lastRequestedEventId);

      return lastRequestIsPowerModesExplanationIntent && isWaitingForServerResponse;
    },
    isWaitingForUserTriggeredActionResponse(state, getters) {
      const { isWaitingForPowerModesExplanation } = getters;

      return state.isWaitingForResponseToAudio
        || state.isWaitingForResponseToText
        || isWaitingForPowerModesExplanation;
    },
    allTechnologiesHaveBeenStartedPlaying(state, getters, rootState, rootGetters) {
      const currentModelCode = rootGetters.getSelectedModel.code;

      const seenTechnologyLength = state.seenTechnologyIntents[currentModelCode].length;
      const allTechnologyLength = bmwTechnologyIntents[currentModelCode].length;

      return seenTechnologyLength === allTechnologyLength;
    },
  },
};
