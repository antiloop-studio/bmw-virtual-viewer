import Bus from '@/store/bus/Bus';
import debounce from '../../debounce';

let manager = null;

export default {
  namespaced: true,
  state: {
    isManagerInitialized: false,
    isHDModelLoaded: false,
    experienceStarted: false,
    isInsideCar: false,
    isConfiguringRealScale: false,
    currentAnimationId: undefined,
    seenAnimations: [],
    personalizationPreferences: {
      0: 0,
      1: 0,
      2: 0,
      3: 0,
    },
    visitedStareDetails: {
      'X1': [],
      '330E': [],
      'X5': [],
    },
  },
  mutations: {
    setIsManagerInitialized(state, status) {
      state.isManagerInitialized = status;
    },
    setAnimationId(state, animationId) {
      state.currentAnimationId = animationId;

      if (animationId !== undefined) {
        state.seenAnimations.push(animationId);
      }
    },
    setIsHDModelLoaded(state, status) {
      state.isHDModelLoaded = status;
    },
    setExperienceStarted(state, status) {
      state.experienceStarted = status;
    },
    setIsInsideCar(state, status) {
      state.isInsideCar = status;
    },
    setIsConfiguringRealScale(state, status) {
      state.isConfiguringRealScale = status;
    },
    clearPersonalizationPrefs(state) {
      state.personalizationPreferences = {
        0: 0,
        1: 0,
        2: 0,
        3: 0,
      };
    },
    setPersonalizationPrefs(state, { propertyIndex, optionIndex }) {
      state.personalizationPreferences = {
        ...state.personalizationPreferences,
        [propertyIndex]: optionIndex,
      };
    },
    markStareDetailVisited(state, { modelCode, stareTargetIntentEvent }) {
      state.visitedStareDetails[modelCode].push(stareTargetIntentEvent);
    },
  },
  actions: {
    startExperience({ state, commit, dispatch }, model) {
      commit('setIsHDModelLoaded', false);

      if (!state.isManagerInitialized) {
        dispatch('initializeManager');
        commit('setIsManagerInitialized', true);
      }

      manager.startExperience(model);
    },

    initializeManager({ commit, dispatch }) {
      manager = new window.ARManager.ARManager('camerafeed');
      manager.mute(true);

      manager.onDisplay8ThWallLoadingUi((display) => {
        if (display) {
          Bus.$emit('showWhiteOverlayLoadingIndicator');
        } else {
          Bus.$emit('hideWhiteOverlayLoadingIndicator');
        }
      });

      manager.onExperienceLoaded(() => {
        Bus.$emit('onExperienceLoaded');
        commit('setExperienceStarted', true);
      });
      manager.onHDCarLoaded(() => {
        Bus.$emit('onHDCarLoaded');
        commit('setIsHDModelLoaded', true);
      });
      manager.onAnimationsLoaded(() => {
        Bus.$emit('onAnimationsLoaded');
      });
      manager.onAnimationFinished(() => {
        dispatch('handleAnimationFinishedEvent');
      });
      manager.onHotspotActivated((hotspotID) => {
        Bus.$emit('onHotspotActivated', hotspotID);
      });
      manager.onHotspotDeactivated(() => {
        Bus.$emit('onHotspotDeactivated');
      });
      manager.onStaringAtDetailStart((stareTargetIntentEvent) => {
        dispatch('userStartedStaringAtDetail', stareTargetIntentEvent);
      });
      manager.onStaringAtDetailFinished(() => {
        Bus.$emit('onStaringAtDetailFinished');
      });
      manager.onEnterCar(() => {
        commit('setIsInsideCar', true);
        Bus.$emit('onEnterCar');
      });
      manager.onExitCar(() => {
        commit('setIsInsideCar', false);
        Bus.$emit('onExitCar');
      });
      manager.onDisplayTutorial((status) => {
        Bus.$emit('onDisplayTutorial', status);
      });
      manager.onAdvanceTutorial((stepId) => {
        Bus.$emit('onAdvanceTutorial', stepId);
      });
      manager.onHDCarLoadProgress((value) => {
        Bus.$emit('onHDCarLoadProgress', value);
      });
      manager.onDisplayLoading((display) => {
        Bus.$emit('onDisplayLoading', display);
      });
    },

    intentDetected(context, intentID) {
      if (manager) {
        manager.intentDetected(intentID);
      }
    },
    loadCar({ commit }, model) {
      if (manager) {
        commit('setIsHDModelLoaded', false);
        manager.loadCar(model);
        commit('clearPersonalizationPrefs');
      }
    },
    playAnimation({ commit }, animationID) {
      if (manager) {
        commit('setAnimationId', animationID);

        Bus.$emit('inactiveBottomMenuComponent');
        manager.playAnimation(animationID);
      }
    },
    stopAnimation({ getters, dispatch }) {
      if (manager && getters.isAnimationPlaying) {
        manager.stopCurrentAnimation();
        dispatch('handleAnimationInterruptedEvent');
      }
    },
    mute(context, mutedState) {
      if (manager && (typeof manager.mute) === 'function') {
        manager.mute(mutedState);
      }
    },
    handleAnimationFinishedEvent({ commit }) {
      commit('setAnimationId', undefined);
      Bus.$emit('inactiveBottomMenuComponent');
    },
    handleAnimationInterruptedEvent({ commit }) {
      commit('setAnimationId', undefined);
      Bus.$emit('inactiveBottomMenuComponent');
    },
    showHotspots({ dispatch }) {
      if (manager) {
        dispatch('debouncedSetHotspotVisibility', true);
      }
    },
    hideHotspots({ dispatch }) {
      if (manager) {
        dispatch('debouncedSetHotspotVisibility', false);
      }
    },
    debouncedSetHotspotVisibility: debounce((context, show) => {
      manager.showHotspots(show);
    }, 1500),
    closeCurrentHotspot({ dispatch, rootGetters }) {
      const currentHotspotId = rootGetters.getCurrentHotspotId;
      if (manager && currentHotspotId !== null && currentHotspotId !== undefined) {
        manager.closeHotspot(currentHotspotId);
        dispatch('setCurrentHotspotId', null, { root: true });
      }
    },
    personalize({ commit }, { property, option }) {
      if (manager) {
        commit('setPersonalizationPrefs', {
          propertyIndex: property,
          optionIndex: option,
        });
        manager.personalize(property, option);
      }
    },
    showTutorial(context, stepId) {
      if (manager) {
        manager.showTutorial(stepId);
      }
    },
    startRealScale({ commit, dispatch }) {
      if (manager) {
        manager.startRealScale();
        commit('setIsConfiguringRealScale', true);
        dispatch('setConfirmAction', () => {
          dispatch('WebGLState/stopRealScale', undefined, { root: true });
          dispatch('launchEvent', '13_4', { root: true });
        }, { root: true });
      }
    },
    stopRealScale({ commit, dispatch }) {
      if (manager) {
        manager.stopRealScale();
        commit('setIsConfiguringRealScale', false);
        dispatch('setConfirmAction', null, { root: true });
      }
    },
    userStartedStaringAtDetail({
      commit, getters, rootGetters, dispatch,
    }, stareTargetIntentEvent) {
      if (typeof stareTargetIntentEvent !== 'string' || !stareTargetIntentEvent) {
        return;
      }
      if (rootGetters['PlayerState/getIsMuted']) {
        return;
      }

      const visitedIntents = rootGetters.getIntents;
      if (!visitedIntents.includes('0.1')) {
        return;
      }

      const isJourneyPaused = rootGetters['JourneyState/shouldJourneyBePaused'];
      const { visitedStareDetails } = getters;
      const detailAlreadyVisited = visitedStareDetails.includes(stareTargetIntentEvent);

      if (isJourneyPaused || detailAlreadyVisited) {
        return;
      }

      const selectedModel = rootGetters.getSelectedModel;
      commit('markStareDetailVisited', { modelCode: selectedModel.code, stareTargetIntentEvent });
      dispatch('launchEvent', stareTargetIntentEvent, { root: true });
    },
  },
  getters: {
    currentAnimationId(state) {
      return state.currentAnimationId;
    },
    isAnimationPlaying(state) {
      return state.currentAnimationId !== undefined;
    },
    seenAnimations(state) {
      return state.seenAnimations;
    },
    isInsideCar(state) {
      return state.isInsideCar;
    },
    isHDModelLoaded(state) {
      return state.isHDModelLoaded;
    },
    getIsConfiguringRealScale(state) {
      return state.isConfiguringRealScale;
    },
    getPersonalizationPrefs(state) {
      return state.personalizationPreferences;
    },
    visitedStareDetails(state, getters, rootState, rootGetters) {
      const selectedModel = rootGetters.getSelectedModel;

      return state.visitedStareDetails[selectedModel.code];
    },
  },
};
