class PlayerService {
  constructor() {
    this.audio = new Audio();
    this.mute();
  }

  audio = null;

  canResume = false;

  playFile({ audioUrl, autoPlay }, callback = () => { }) {
    this.stop();

    if (!this.audio) {
      this.audio = new Audio();
    }

    this.audio.src = audioUrl;
    this.audio.volume = 1;
    this.audio.onplay = () => console.log('PlayerService/onplay');
    this.audio.onerror = (error) => console.log('PlayerService/onerror', error);
    this.audio.onvolumechange = () => console.log('PlayerService/onvolumechange');
    this.audio.onwaiting = () => console.log('PlayerService/onwaiting');
    this.audio.onended = () => {
      console.log('PlayerService/onended');
      this.canResume = false;
      callback();
    };

    this.canResume = true;
    this.audio.play()
      .then(() => {
        if (!autoPlay) {
          this.audio.pause();
        }
      })
      .catch(() => console.log('PlayerService/failed to start playing'));
  }

  playBlob(blob) {
    this.stop();

    if (!this.audio) {
      this.audio = new Audio();
    }

    this.audio.src = URL.createObjectURL(blob);
    this.audio.volume = 1;
    this.audio.onplay = () => console.log('PlayerService/onplay');
    this.audio.onerror = (error) => console.log('PlayerService/onerror', error);
    this.audio.onvolumechange = () => console.log('PlayerService/onvolumechange');
    this.audio.onwaiting = () => console.log('PlayerService/onwaiting');
    this.audio.onended = () => {
      console.log('PlayerService/onended');
      this.canResume = false;
    };

    this.audio.play().catch(() => console.log('PlayerService/failed to start playing'));
    this.canResume = true;
  }

  resume() {
    if (this.audio && this.canResume) {
      this.audio.play();
      this.audio.volume = 1;
    }
  }

  pause() {
    if (this.audio) {
      this.audio.pause();
    }
  }

  stop() {
    if (this.audio) {
      this.audio.pause();
      this.canResume = false;
    }
  }

  mute() {
    if (this.audio) {
      this.audio.muted = true;
    }
  }

  unmute() {
    if (this.audio) {
      this.audio.muted = false;
    }
  }
}

export default new PlayerService();
