import PlayerService from '@/store/player/PlayerService';

export default {
  namespaced: true,
  state: {
    isPlaying: false,
    lastAudioGotStopped: false,
    currentAudio: undefined,
    isMuted: true,
  },
  mutations: {
    setIsPlaying(state, isPlaying) {
      state.isPlaying = isPlaying;
    },
    setLastAudioGotStopped(state, lastAudioGotStopped) {
      state.lastAudioGotStopped = lastAudioGotStopped;
    },
    setCurrentAudio(state, audioFilename) {
      state.currentAudio = audioFilename;
    },
    setMutedStatus(state, isMuted) {
      state.isMuted = isMuted;
    },
  },
  actions: {
    play({ commit, getters }, audioFilename) {
      const { canPlay } = getters;

      commit('setLastAudioGotStopped', false);
      commit('setIsPlaying', canPlay);
      commit('setCurrentAudio', audioFilename);

      const options = {
        audioUrl: audioFilename,
        autoPlay: canPlay,
      };
      PlayerService.playFile(options, () => {
        commit('setIsPlaying', false);
        commit('setCurrentAudio', undefined);
      });
    },
    resume({ commit, getters }) {
      const { canPlay } = getters;

      if (PlayerService.canResume && canPlay) {
        commit('setIsPlaying', true);

        PlayerService.resume();
      }
    },
    pause({ commit }) {
      commit('setIsPlaying', false);

      PlayerService.pause();
    },
    stop({ commit }) {
      commit('setIsPlaying', false);
      commit('setCurrentAudio', undefined);
      commit('setLastAudioGotStopped', true);

      PlayerService.stop();
    },
    setMutedStatus({ commit, dispatch }, shouldMute) {
      commit('setMutedStatus', shouldMute);
      dispatch('WebGLState/mute', shouldMute, { root: true });

      if (shouldMute) {
        PlayerService.mute();
      } else {
        PlayerService.unmute();
      }
    },
  },
  getters: {
    getIsPlaying(state) {
      return state.isPlaying;
    },
    getCurrentAudio(state) {
      return state.currentAudio;
    },
    getIsMuted(state) {
      return state.isMuted;
    },
    canPlay(state, getters, rootState, rootGetters) {
      return rootGetters.getWindowHasFocus;
    },
    lastIntentIsMuteRequest(state, getters, rootState) {
      const { lastIntent } = rootState.ApiState;

      return lastIntent === '15.0';
    },
  },
};
