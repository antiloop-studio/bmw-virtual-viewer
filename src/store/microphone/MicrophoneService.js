import RecorderService from './RecorderService';

class MicrophoneService {
  isRecording = false;

  recorderService = null;

  getMicrophonePermission() {
    return new Promise((resolve) => {
      if (!navigator.permissions || !navigator.permissions.query) {
        resolve(null);
        return;
      }

      navigator.permissions.query({ name: 'microphone' })
        .then((result) => {
          if (result.state === 'granted') {
            resolve(true);
          }
          if (result.state === 'prompt') {
            resolve(null);
          }
          if (result.state === 'denied') {
            resolve(false);
          }
          resolve(false);
        })
        .catch(() => resolve(null));
    });
  }

  askMicrophonePermission() {
    return new Promise((resolve) => {
      navigator.mediaDevices.getUserMedia({ audio: true, video: false })
        .then((result) => resolve(!!result))
        .catch(() => resolve(false));
    });
  }

  startRecording(callback) {
    if (!this.recorderService) {
      this.recorderService = new RecorderService();
      this.recorderService.em.addEventListener('recording', (event) => {
        callback(event.detail.recording);
      });
    }

    this.recorderService.startRecording();
    this.isRecording = true;
  }

  stopRecording() {
    if (this.recorderService && this.isRecording) {
      this.recorderService.stopRecording();
      this.isRecording = false;
    }
  }
}

export default new MicrophoneService();
