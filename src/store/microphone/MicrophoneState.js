import MicrophoneService from './MicrophoneService';

export default {
  state: {
    hasMicroPermission: undefined,
    isRecording: false,
    lastRecord: undefined,
    lastRecordingStartedAt: 0,
    recordingOrRecentlyFinishedRecording: false,
    setRecentlyFinishedRecordingTimeout: null,
  },
  mutations: {
    updateMicroPermission(state, enabled) {
      state.hasMicroPermission = enabled;
    },
    updateIsRecording(state, status) {
      state.isRecording = status;

      if (status) {
        clearTimeout(state.setRecentlyFinishedRecordingTimeout);
        state.recordingOrRecentlyFinishedRecording = true;
      } else {
        clearTimeout(state.setRecentlyFinishedRecordingTimeout);
        state.setRecentlyFinishedRecordingTimeout = setTimeout(() => {
          state.recordingOrRecentlyFinishedRecording = false;
        }, 10000);
      }
    },
    updateLastRecord(state, blob) {
      state.lastRecord = blob;
    },
    storeLastRecordingStartedAtTimestamp(state, timestamp) {
      state.lastRecordingStartedAt = timestamp;
    },
  },
  actions: {
    async checkPreviusMicrophonePermissions() {
      const hasPermission = await MicrophoneService.getMicrophonePermission();
      this.commit('updateMicroPermission', hasPermission);
    },
    async getMicrophonePermissions({ dispatch }) {
      const gotPermission = await MicrophoneService.askMicrophonePermission();
      if (gotPermission) {
        dispatch('analyticsEventHit', {
          category: 'ONBOARDING', action: 'Click', event: 'Click on OK', label: 'OK', value: 'Microphone',
        });
      } else {
        dispatch('analyticsEventHit', {
          category: 'ONBOARDING', action: 'Click', event: "Click on Don't Allow", label: "Don't Allow", value: 'Microphone',
        });
      }
      this.commit('updateMicroPermission', gotPermission);
    },
    startRecording({ dispatch, commit }) {
      commit('updateIsRecording', true);
      commit('storeLastRecordingStartedAtTimestamp', Date.now());
      MicrophoneService.startRecording((blob) => {
        dispatch('handleRecordingFinished', blob);
      });
    },
    handleRecordingFinished({ state, commit, dispatch }, blob) {
      commit('updateLastRecord', blob);
      dispatch('launchAudio', blob);

      if (state.lastRecordingStartedAt > 0) {
        const durationInMs = Date.now() - state.lastRecordingStartedAt;
        const durationInSeconds = durationInMs / 1000;
        dispatch('analyticsEventHit', {
          category: 'ALL', action: 'Hold', event: 'Hold Voice Button', label: 'Voice', value: `${durationInSeconds}s`,
        });
      }
    },
    stopRecording() {
      this.commit('updateIsRecording', false);
      MicrophoneService.stopRecording();
    },
  },
  getters: {
    hasMicroPermission(state) {
      return state.hasMicroPermission;
    },
    getIsRecording(state) {
      return state.isRecording;
    },
    isRecordingOrRecentlyFinishedRecording(state) {
      return state.recordingOrRecentlyFinishedRecording;
    },
    getLastRecord(state) {
      return state.lastRecord;
    },
  },
};
