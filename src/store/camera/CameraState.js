import Camera from './CameraService';

export default {
  state: {
    hasCameraPermission: undefined,
  },
  mutations: {
    updateCameraPermission(state, allowed) {
      state.hasCameraPermission = allowed;
    },
  },
  actions: {
    async checkPreviusCameraPermissions() {
      const hasPermission = await Camera.getCameraPermission();
      this.commit('updateCameraPermission', hasPermission);
    },
    async getCameraPermissions({ dispatch }) {
      const gotPermission = await Camera.askCameraPermission();
      if (gotPermission) {
        dispatch('analyticsEventHit', {
          category: 'ONBOARDING', action: 'Click', event: 'Click on OK', label: 'OK', value: 'Camera',
        });
      } else {
        dispatch('analyticsEventHit', {
          category: 'ONBOARDING', action: 'Click', event: "Click on Don't Allow", label: "Don't Allow", value: 'Camera',
        });
      }

      this.commit('updateCameraPermission', gotPermission);
    },
  },
  getters: {
    hasCameraPermission(state) {
      return state.hasCameraPermission;
    },
  },
};
