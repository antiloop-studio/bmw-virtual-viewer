export default class CameraService {
  static getCameraPermission() {
    return new Promise((resolve) => {
      if (!navigator.permissions || !navigator.permissions.query) {
        resolve(null);
        return;
      }

      navigator.permissions.query({ name: 'camera' })
        .then((result) => {
          if (result.state === 'granted') {
            resolve(true);
          }
          if (result.state === 'prompt') {
            resolve(null);
          }
          if (result.state === 'denied') {
            resolve(false);
          }
          resolve(false);
        })
        .catch(() => resolve(null));
    });
  }

  static askCameraPermission() {
    return new Promise((resolve) => {
      navigator.mediaDevices.getUserMedia({ audio: false, video: true })
        .then((result) => resolve(!!result))
        .catch(() => resolve(false));
    });
  }
}
