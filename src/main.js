import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import './styles/_index.scss';
import App from './App.vue';
import store from './store';

Vue.use(VueAxios, axios);

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');

document.addEventListener('touchmove', (event) => {
  if (event.touches.length > 1 && event.scale !== 1) {
    event.preventDefault();
  }
}, { passive: false });
