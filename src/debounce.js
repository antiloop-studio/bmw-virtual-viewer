const debounce = (mainFunction, wait = 250) => {
  let timer;
  const debouncedFunction = (...args) => {
    if (!timer) {
      mainFunction(...args);
      timer = setTimeout(() => {
        clearTimeout(timer);
        timer = null;
      }, wait);
      return;
    }
    if (timer) {
      clearTimeout(timer);
      timer = null;
    }
    timer = setTimeout(() => {
      mainFunction(...args);
      timer = null;
    }, wait);
  };
  return debouncedFunction;
};

export default debounce;
