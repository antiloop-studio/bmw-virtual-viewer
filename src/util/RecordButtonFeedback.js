const pressFeedbackFilePath = '/sounds/button-press.mp3';
const releaseFeedbackFilePath = '/sounds/button-release.mp3';

class RecordButtonFeedback {
  pressAudio = null;
  releaseAudio = null;

  constructor() {
    this.pressAudio = new Audio();
    this.pressAudio.src = pressFeedbackFilePath;
    this.pressAudio.volume = 1;

    this.releaseAudio = new Audio();
    this.releaseAudio.src = releaseFeedbackFilePath;
    this.releaseAudio.volume = 1;
  }

  async playPressFeedbackAsync() {
    return new Promise((resolve) => {
      this.pressAudio.currentTime = 0.0;
      this.pressAudio.onended = () => {
        resolve();
      };

      this.pressAudio.onerror = () => {
        resolve();
      };

      this.pressAudio.play().catch(() => {
        resolve();
      });

      this.releaseAudio.muted = true;
      this.releaseAudio.play();
    });
  }

  async playReleaseFeedbackAsync() {
    return new Promise((resolve) => {
      this.releaseAudio.muted = false;
      this.releaseAudio.pause();
      this.releaseAudio.currentTime = 0.0;
      this.releaseAudio.onended = () => {
        resolve();
      };

      this.releaseAudio.onerror = () => {
        resolve();
      };

      this.releaseAudio.play().catch(() => {
        resolve();
      });
    });
  }
}

export default new RecordButtonFeedback();
