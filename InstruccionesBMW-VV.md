# Estructura del proyecto

## Backend

El backend que hay desarrollado es temporal, debe optimizarse para pasarlo a producción.

Está desarrollado con bottle (python), y se encuentra en la carpeta server/:
```
- cors.py           para desactivar CORS
- dialogflow.py     son las funciones principales de comunicación con dialogflow
- server.py         el server
- views.py          las vistas
[.env]
[credentials/]
```

#### Entorno y autenticación del backend

Dentro de server hay que crear un archivo `.env` con las credenciales y las variables de entorno del server (cambiarán según entorno):
```
GOOGLE_APPLICATION_CREDENTIALS="server/credentials/BMW-AR-TEST2-4880d5926cc2.json"
SERVER_HOST="localhost"
SERVER_PORT=8000
DIST_FOLDER=public
```

La variable GOOGLE_APPLICATION_CREDENTIALS hace referencia al archivo donde se encuentran las variables de authenticación de google cloud (se ha elegido este método de authenticación). Dicho archivo tendrá una estructura tal que:

```
{
  "type": "service_account",
  "project_id": "bmw-ar-dmodjf",
  "private_key_id": "4860c92a983447005deb9ff6ad72d48a9061f9c7",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADA...",
  "client_email": "dialogflow-okudhv@bmw-ar-dmodjf.iam.gserviceaccount.com",
  "client_id": "110340257959660048768",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/..."
}
```

#### API

Las requests desde el front al back deben pasar ciertos parametros:
```
project               el project de dialogflow, por defecto 'bmw-ar-test2-rssdia'
session               la session del usuario, (para identificarlo)
text, file, o event   el texto, audio o evento que hay que triggear
locale                el idioma y localización (por defecto 'en-US')
```

#### Dialogflow audio response

Las request a dialogflow normalmente devolverán un audio, que habrá que reproducir en la app. Para ello habrá que guardar el binario que devuelve la API de dialogflow en un archivo y luego pasarle el path al frontend. Esto se realiza en la función `compose_dialogflow_response()` de dialogflow.py, y los audios se guardan en la carpeta public/audio/.

Para ahorrar un poco de procesamiento, si el que devuelve dialogflow ya está almacenado en el server, no se vuelve a guardar [Al cambiar alguna configuración de dialogflow es recomendable borrar los audios existentes]


## Frontend

El proyecto está desarrollado con Vue (v2) y hace un uso intensivo de Vuex y APIs para reproducir/grabar audio (es la parte más problemática).

En src/constants.js se encuentran las constantes más generales de los componentes, como las imágenes de los sliders, del discovery, etc.

### Variables de entorno

Para los diferentes entornos (local, desarrollo, producción) hay que crear los archivos .env.dev, .env.pro, .env.local, que tendrán las siguientes variables (con distintos valores según el archivo):
```
VUE_APP_EIGHTWALL_KEY=pOx4VGj8ghNXAk3hlk9mKGsav6DxUDP8oE9s12TneKCh8RaAQE0RVrayAbpIsBEwfpuI6L
VUE_APP_API_URL=https://antiloop-test-api.dataira.com
VUE_APP_DIALOGFLOW_PROJECT=bmw-ar-dmodjf
VUE_APP_DIALOGFLOW_LOCALE=en-GB
```

#### Gestión de los estados

En la carpeta store están las funciones para manejar el estado y algunos servicios asociados:
```
api/            para la comunicación con el backend
bus/            un bus general para lanzar ciertos eventos
camera/         para el acceso a la cámara del dispositivo
cookies/        para las cookies
microphone/     para el acceso al micrófono y la grabación de audio
player/         para la reporducción de audio y el control del volumen (mute on/off)
timer/          el timer (para el user journey)
views/          para el manejo de las views y los componentes
webgl/          para la comunicación con webGL
```

#### Grabación de audio

Esta es la parte más sensible del proyecto.
Cada dispositivo (android/iphone) graba el audio a su manera, por lo que hay que utilizar una librería para gestionarlo (detalles en MicrophoneService.js). En resumen, para que funcione bien hay que controlar el bufferSize (que depende de la memoria de cada dispositivo), grabar en un canal (mono) y pasar la grabación a wav.

#### Componentes

La parte más sencilla de la app (los permisos y el car selection) se resuelve con views (carpeta components/permissions/ y components/views/). La navegación se resuelve mediante un estado central, que se llama así:
```
this.$store.dispatch('setViewByName', 'permissions-camera'); // nombre definito en constants.js
```

Una vez llegamos a la vista principal MainViewComponent.vue, donde se carga el webGL, los distintos componentes deben activarse o desactivarse en función de los inputs del usuario, de los inputs de dialoglof, y de los inputs del timer (es algo complejo).

La mayoría de componentes, como MainMenuButtonComponent.vue, se pueden activar con una action de su estado:
```
this.$store.dispatch('enableComponent', 'MainMenuButtonComponent');
```

Otros componentes sin embargo tienen distintos subcomponentes (o un mismo componente padre). Es el caso de BottomMenuCharging.vue, BottomMenuCookies.vue, BottomMenuPersonalization.vue, etc, que están "envueltos" en BottomMenuComponent.vue. Para activar estos, primero hay que configurar el componente padre, y luego activarlo:
```
this.$store.dispatch('setComponentContent', {
  component: 'BottomMenuComponent', // componente padre
  content: 'BottomMenuCharging', // componente hijo
});
this.$store.dispatch('enableComponent', 'BottomMenuComponent'); // activar componente padre
```

Esto funciona asi con los contenidos principales (charging network, xray, etc), con los modales y los enlacez externos.

#### Integración del repo de WebGL (bmw)

Para que el repositorio bmw (de webGL) funcione bien al integrarlo es necesario hacer unos cambios en la ruta de los archivos assetsLoaders.js:

Estas lineas:
```
this.setupLoader(this.gltfLoader, './models/', '');
this.setupLoader(this.textureLoader, './images/textures/', '');
this.setupLoader(this.rgbeLoader, './images/textures/', '');
```

Deben cambiarse por estas:
```
this.setupLoader(this.gltfLoader, './armanager/models/', '');
this.setupLoader(this.textureLoader, './armanager/images/textures/', '');
this.setupLoader(this.rgbeLoader, './armanager/images/textures/', '');
```

Una vez hecho el cambio y la build, deben copiarse los archivos de la carpeta [bmw repo]/AppV7/dist/ a [
BMW Virtual Viewer repo]/public/armanager/ (el archivo [bmw repo]/build.sh es el que usaba para esto)

Tened en cuenta que para que los últimos archivos de webGL se carguen hay que eliminar la cache del dispositivo al consultar la página web.

#### Otros apuntes

- En README.md están las instrucciones para montar el entorno de desarrollo y hacer el deploy, pero están un poco desactualizadas (no mucho)

- El componente DevMenuComponent.vue es una consola general de estado, que permite ver el estado general de la aplicación. Este componente, entre otros, permite reproducir el último audio grabado por el usuario, que ayuda a saber si el audio enviado a dialogflow es correcto o está cortado.

- Los archivos de 8thwall deben insertarse en el html una vez se han concedido los permisos de cámara, micrófono y acelerómetro, ya que estos archivos los solicitarán automáticamente.

- Para facilitar el deploy se ha creado el archivo deploy.sh, que se ejecuta en el servidor y se encarga de bajar los últimos cambios del repo, eliminar los audios y reiniciar los procesos (yo utilizaba uwsgi y nginx)

- Los estilos generales están en la carpeta styles/, y los específicos de cada componente en su propio archivo.

- De las dependencias actuales del front se pueden eliminar algunas:
```
"axios": "^0.19.2",
"babylonjs": "4.1.0",                 // se usa para webgl
"babylonjs-loaders": "4.1.0",         // se usa para webgl
"core-js": "3.6.5",
"dialogflow": "^1.2.0",               // se puede eliminar
"flickity": "^2.2.1",                 // se usa para el slider
"howler": "^2.2.0",                   // se usa para reproducir audio
"three": "^0.118.3",                  // se puede eliminar (creo)
"uuid": "^8.1.0",                     // se usa para la sesión de usuario
"vue": "2.6.11",
"vue-axios": "^2.1.5",
"vue-directive-long-press": "^1.1.0", // se usa para manejar el press & hold del botón de grabación
"vue-router": "3.2.0",
"vue-slick-carousel": "^1.0.6",       // se puede eliminar
"vue-slider-component": "^3.2.3",     // se puede eliminar
"vuex": "3.4.0",
"web-audio-recorder-js": "0.0.2"      // se usa para grabar audio
```
