import os
import bottle
import views
from cors import EnableCors
from dotenv import load_dotenv

load_dotenv()

app = application = bottle.default_app()
app.install(EnableCors())

@app.hook('after_request')
def enable_cors():
    # the headers will not be set when the controller raises an error
    # https://github.com/bottlepy/bottle/issues/1125
    bottle.response.headers['Access-Control-Allow-Origin'] = '*'
    bottle.response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    bottle.response.headers['Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

if __name__ == '__main__':
    host = os.environ.get('SERVER_HOST', '0.0.0.0')
    port = os.environ.get('SERVER_PORT', os.environ.get('PORT', 8000))
    bottle.run(app, host=host, port=port)
