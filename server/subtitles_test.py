import json
from subtitles import parse_subtitles


def should_parse_valid_response():
    text = """
    <speak>
        Hi! Welcome to the BMW Virtual Viewer.
        <metadata>
            <subtitles>
                <text time="300">Hello</text>
                <pause time="500"/>
                <text time="400">world!</text>
            </subtitles>
        </metadata>
    </speak>
    """
    
    subtitles = parse_subtitles(text)

    expected = [
        {
            "type": "text",
            "time": 300,
            "content": "Hello"
        },
        {
            "type": "pause",
            "time": 500,
        },
        {
            "type": "text",
            "time": 400,
            "content": "world!"
        },
    ]
    equal_objects(expected, subtitles)

def should_ignore_nodes_without_time():
    text = """
    <speak>
        Hi! Welcome to the BMW Virtual Viewer.
        <metadata>
            <subtitles>
                <text time="200">I'm</text>
                <pause />
                <pause time="600"/>
                <text>Ignored</text>
                <text time="300">valid</text>
            </subtitles>
        </metadata>
    </speak>
    """
    
    subtitles = parse_subtitles(text)

    expected = [
        {
            "type": "text",
            "time": 200,
            "content": "I'm"
        },
        {
            "type": "pause",
            "time": 600,
        },
        {
            "type": "text",
            "time": 300,
            "content": "valid"
        },
    ]
    equal_objects(expected, subtitles)

def should_accept_only_allowed_tags():
    text = """
    <speak>
        Hi! Welcome to the BMW Virtual Viewer.
        <metadata>
            <subtitles>
                <text time="1">Valid text</text>
                <other />
                <pause time="1000"/>
                <video>Ignored</video>
            </subtitles>
        </metadata>
    </speak>
    """
    
    subtitles = parse_subtitles(text)

    expected = [
        {
            "type": "text",
            "time": 1,
            "content": "Valid text"
        },
        {
            "type": "pause",
            "time": 1000,
        },
    ]
    equal_objects(expected, subtitles)

def should_be_empty_when_metadata_is_not_present():
    text = """
    <speak>
        Hi! Welcome to the BMW Virtual Viewer.
    </speak>
    """
    
    subtitles = parse_subtitles(text)

    expected = []
    equal_objects(expected, subtitles)

def should_be_empty_when_subtitles_is_not_present():
    text = """
    <speak>
        Hi! Welcome to the BMW Virtual Viewer.
        <metadata>
            <other>Other metadata</other>
        </metadata>
    </speak>
    """
    
    subtitles = parse_subtitles(text)

    expected = []
    equal_objects(expected, subtitles)

def should_be_empty_when_is_not_valid_xml():
    text = """
    Hi! Welcome to the BMW Virtual Viewer.
    <speak>
        <metadata>
            <subtitles>
                <text time="200">I'm</text>
                <pause time="600"/>
                <text time="300">valid</text>
            </subtitles>
        </metadata>
    </speak>
    """

    subtitles = parse_subtitles(text)

    expected = []
    equal_objects(expected, subtitles)

def should_be_empty_when_speak_is_not_the_root_element():
    text = """
    <metadata>
        <subtitles>
            <text time="200">I'm</text>
            <pause time="600"/>
            <text time="300">valid</text>
        </subtitles>
    </metadata>
    """

    subtitles = parse_subtitles(text)

    expected = []
    equal_objects(expected, subtitles)

def should_be_empty_when_is_just_plain_text():
    text = "Hi! Welcome to the BMW Virtual Viewer."
    
    subtitles = parse_subtitles(text)

    expected = []
    equal_objects(expected, subtitles)


def equal_objects(a, b):
    a = json.dumps(a, sort_keys=True)
    b = json.dumps(b, sort_keys=True)
    if a != b:
        print(a)
        print(b)
        raise Exception('Invalid parsing')

def run_tests():
    should_parse_valid_response()
    should_ignore_nodes_without_time()
    should_be_empty_when_metadata_is_not_present()
    should_be_empty_when_subtitles_is_not_present()
    should_be_empty_when_is_not_valid_xml()
    should_be_empty_when_speak_is_not_the_root_element()
    should_be_empty_when_is_just_plain_text()

run_tests()
