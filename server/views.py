import json
import os
from bottle import route, request, response, post, get, put, delete, template, static_file
from dialogflow import detect_intent_event, detect_intent_texts, detect_intent_audio

DIST_FOLDER = os.environ.get('DIST_FOLDER')
AUDIO_DIR = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), DIST_FOLDER, 'audio')

@route('/', method=['GET'])
def index_view():
    return template('<h1>{{message}}</h1>', message='404')

@route('/event', method=['POST', 'OPTIONS', 'GET'])
@route('/event/', method=['POST', 'OPTIONS', 'GET'])
def dialogflow_event_view():
    response.headers['Content-Type'] = 'application/json'

    dialog_response = detect_intent_event(
        request.json.get('project', 'bmw-ar-test2-rssdia'),
        request.json['session'],
        request.json['event'],
        request.json.get('locale', 'en-US'),
    )

    return json.dumps(dialog_response)

@route('/text', method=['POST', 'OPTIONS', 'GET'])
@route('/text/', method=['POST', 'OPTIONS', 'GET'])
def dialogflow_text_view():
    response.headers['Content-Type'] = 'application/json'

    dialog_response = detect_intent_texts(
        request.json.get('project', 'bmw-ar-test2-rssdia'),
        request.json['session'],
        str(request.json['text']),
        request.json.get('locale', 'en-US'),
    )

    return json.dumps(dialog_response)

@route('/audio', method=['POST', 'OPTIONS', 'GET'])
@route('/audio/', method=['POST', 'OPTIONS', 'GET'])
def dialogflow_audio_view():
    dialog_response = detect_intent_audio(
        request.forms.get("project", 'bmw-ar-test2-rssdia'),
        request.forms.get("session"),
        request.files['file'].file.read(),
        request.forms.get("locale", 'en-US'),
    )

    response.headers['Content-Type'] = 'application/json'
    return json.dumps(dialog_response)

@route('/audio/<filename:path>')
def send_static(filename):
    response = static_file(filename, root=AUDIO_DIR)
    response.set_header('Access-Control-allow-Origin', '*')
    response.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, OPTIONS')
    response.set_header('Access-Control-Allow-Headers', 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
    return response
