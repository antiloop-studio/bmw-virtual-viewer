from bs4 import BeautifulSoup

def parse_subtitles(text):    
    soup = BeautifulSoup(text, 'xml')
    subtitles = []

    if not soup.speak or not soup.speak.metadata or not soup.speak.metadata.subtitles:
        return subtitles

    allowed_tags = ['text', 'pause']
    for element in soup.speak.metadata.subtitles.find_all(allowed_tags):
        if not element.has_attr('time'):
            continue

        subtitle = {
            'type': element.name,
            'time': int(element['time']),
        }
        if element.text:
            subtitle['content'] = element.text

        subtitles.append(subtitle)
    
    return subtitles
