import json
import base64
from requests_toolbelt import MultipartDecoder
from src.dialogflow import detect_intent_event, detect_intent_audio, detect_intent_texts


def event(event, context):
    is_request = 'body' in event
    body = json.loads(event['body']) if is_request else event

    validate_params(body)
    assert isinstance(body.get('event'), str), 'event param is required'

    response = detect_intent_event(
        body['project'],
        body['session'],
        body['event'],
        body['locale'],
    )

    return json.dumps(response)

def text(event, context):
    is_request = 'body' in event
    body = json.loads(event['body']) if is_request else event

    validate_params(body)
    assert isinstance(body.get('text'), str), 'text param is required'

    response = detect_intent_texts(
        body['project'],
        body['session'],
        body['text'],
        body['locale'],
    )

    return json.dumps(response)

def audio(event, context):
    is_request = 'body' in event
    content_type = event.get('headers', {}).get('content-type', '')

    if not is_request:
        raise Exception('Only HTTP requests are supported')

    if 'multipart/form-data' not in content_type:
        raise Exception('Only multipart/form-data requests are supported')

    body = parse_uploaded_audio_body(content_type, event['body'])

    validate_params(body)

    response = detect_intent_audio(
        body['project'],
        body['session'],
        body['file'],
        body['locale'],
    )

    return json.dumps(response)

def echo(event, context):
    data = None
    content_type = event['headers']['content-type']

    if 'multipart/form-data;' in content_type:
        data = parse_uploaded_audio_body(content_type, event['body'])

    response = {
        'event': event,
        'data': data,
    }

    return json.dumps(response)

def validate_params(body):
    print('body:')
    print(body)
    assert isinstance(body.get('project'), str), 'project param is required'
    assert isinstance(body.get('session'), str), 'session param is required'
    assert isinstance(body.get('locale'), str), 'locale param is required'

def parse_uploaded_audio_body(content_type, raw_body):
    data = {}
    raw_data = base64.b64decode(raw_body).decode('iso-8859-1').encode('utf-8')
    decoder = MultipartDecoder(raw_data, content_type)

    for part in decoder.parts:
        header = part.headers[b'Content-Disposition'].decode('utf-8')

        if 'name="project"' in header:
            data['project'] = part.text
        if 'name="session"' in header:
            data['session'] = part.text
        if 'name="file"' in header:
            data['file'] = part.text.encode('iso-8859-1')
        if 'name="locale"' in header:
            data['locale'] = part.text

    return data
