import os
import re
import base64
import boto3
import botocore
import json
import dialogflow_v2 as dialogflow
import hashlib
from google.protobuf.json_format import MessageToJson
from src.subtitles import parse_subtitles

AUDIO_FILES_BUCKET = os.environ.get('AUDIO_FILES_BUCKET')
AUDIO_FILES_FOLDER = 'audio/'

session_client = dialogflow.SessionsClient()
s3 = boto3.client('s3')

output_audio_config = dialogflow.types.OutputAudioConfig(
    audio_encoding=dialogflow.enums.OutputAudioEncoding.OUTPUT_AUDIO_ENCODING_MP3,
    sample_rate_hertz=48000
)

def compose_dialogflow_response(response):
    query_result = json.loads(MessageToJson(response.query_result))
    text_response = response.query_result.fulfillment_text
    subtitlesx = parse_subtitles(text_response)

    if text_response:
        audio_file_name = AUDIO_FILES_FOLDER
        audio_file_name += re.sub('[\W_]+', '', text_response)[0:50]
        audio_file_name += '_'
        audio_file_name += hashlib.md5(str.encode(text_response)).hexdigest()
        audio_file_name += '.mp3'

        try:
            # fails when the file doesn't exists
            s3.head_object(Key=audio_file_name, Bucket=AUDIO_FILES_BUCKET)
        except botocore.exceptions.ClientError:
            print('Uploading file to S3: ' + AUDIO_FILES_BUCKET + '/' + audio_file_name)
            s3.put_object(
                Key=audio_file_name,
                Bucket=AUDIO_FILES_BUCKET,
                Body=response.output_audio,
            )
    else:
        audio_file_name = None

    return {
        'fulfillment_text': text_response,
        'audio_file_name': audio_file_name,
        'query_result': query_result,
        'subtitles': subtitlesx,
    }


def detect_intent_event(project_id, session_id, event, language_code='en-US'):
    """
    Returns the result of detect intent with texts as inputs.
    https://cloud.google.com/dialogflow/docs/quick/api
    """
    session = session_client.session_path(project_id, session_id)

    event_input = dialogflow.types.EventInput(
        name=event, language_code=language_code)

    query_input = dialogflow.types.QueryInput(event=event_input)

    response = session_client.detect_intent(
        session=session,
        query_input=query_input,
        output_audio_config=output_audio_config,
    )

    return compose_dialogflow_response(response)


def detect_intent_texts(project_id, session_id, text, language_code='en-US'):
    """
    Returns the result of detect intent with texts as inputs.
    https://cloud.google.com/dialogflow/docs/quick/api
    """
    session = session_client.session_path(project_id, session_id)

    text_input = dialogflow.types.TextInput(
        text=text, language_code=language_code)

    query_input = dialogflow.types.QueryInput(text=text_input)

    response = session_client.detect_intent(
        session=session,
        query_input=query_input,
        output_audio_config=output_audio_config,
    )

    return compose_dialogflow_response(response)


def detect_intent_audio(project_id, session_id, audio_file, language_code='en-US'):
    """
    Returns the result of detect intent with an audio file as input.
    https://cloud.google.com/dialogflow/docs/how/detect-intent-audio
    """
    session = session_client.session_path(project_id, session_id)

    audio_config = dialogflow.types.InputAudioConfig(
        language_code=language_code,
        # audio_encoding=dialogflow.enums.AudioEncoding.AUDIO_ENCODING_LINEAR_16,
        # sample_rate_hertz=16000
    )

    # contexts = []
    # for context_name in ['model_select', 'model_select_fallback', '31modelselect-followup']:
    #     contexts.append(dialogflow.types.Context(
    #         name='projects/{}/agent/sessions/{}/contexts/{}'.format(project_id, session_id, context_name)))

    query_input = dialogflow.types.QueryInput(audio_config=audio_config)
    # query_params = dialogflow.types.QueryParameters(contexts=contexts)

    response = session_client.detect_intent(
        session=session, query_input=query_input,
        # query_params=query_params,
        input_audio=audio_file,
        output_audio_config=output_audio_config,
    )

    return compose_dialogflow_response(response)
