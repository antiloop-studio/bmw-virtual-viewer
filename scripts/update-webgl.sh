#!/bin/bash
# Update the armanager code (often referred as WebGL) coming from the `bmw` repository.
# The script assumes that the repositories (current and `bmw`) are cloned as siblings.
# Use `npm run update-webgl` to run it.

set -e

echo '📥 Update `bmw` repository'
cd ../bmw/AppV7
git fetch -f
git pull

echo '🔧 Build new armanager version'
# remove directory because the build was caching files with just the name changed (file.ext -> File.ext)
rm -r dist
npm install
npm run build

echo '💾 Copy built artifacts'
cd ../../bmw-virtual-viewer
# remove old files and stage them to trick git when there are file name changes (file.ext -> File.ext)
rm -r public/armanager
git add public/armanager
cp -r ../bmw/AppV7/dist/ public/armanager/
git add public/armanager/*

echo '✅ Updated successfully, now check the changes locally and commit them'
