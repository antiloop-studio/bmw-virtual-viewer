module.exports = {
  devServer: {
    https: true,
    proxy: 'http://localhost:8000',
  },
  filenameHashing: true,
  chainWebpack: (config) => {
    config.plugins.delete('preload');
    config.plugins.delete('prefetch');
  },
};
